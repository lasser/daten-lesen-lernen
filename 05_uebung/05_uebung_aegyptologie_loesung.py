# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _Lizenz: Das folgende Lehrmaterial kann unter einer [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) Lizenz frei verwendet, verbreitet und modifiziert werden._   
#  _Authoren: Jana Lasser (jana.lasser@ds.mpg.de)_  
#  _Das Lehrmaterial wurde im Zuge des Projektes "Daten Lesen Lernen", gefördert vom Stifterverband und der Heinz Nixdorf Stiftung erstellt._

# # Lösungen zu Übung 05 - Ägyptologie: Daten finden und einlesen und mit Histogrammen visualisieren

# <a name="top"></a>Inhalt
# ---
# * [Daten beschaffen](#daten_beschaffen)
# * [Unicode](#unicode)
# * [Histogramme](#histogramme) 

# <a name="funktionen"></a>1. Daten beschaffen
# ---

# +
# 1.B
# -

# Das Git-Repository ist das öffentlich zugängliche Repository der Seite copticscriptorium.org. Auf dieser Seite werden koptische Texte annotiert und aufbereitet zur Verfügung gestellt. Das [Projekt coptic scriptorium](https://linguistics.georgetown.edu/research/grants/coptic-scriptorium/) wird von der Abteilung Linguistik der Georgetown University vorangetrieben. Es ist wichtig, sich einen Überblick über die Herkunft der Daten zu verschaffen, um ihre Qualität und Zuverlässigkeit einschätzen zu können. Ist die Herkunft der Daten gut dokumentiert? Sind Metadaten vorhanden? Gibt es wissenschaftliche Publikationen zu den Daten? All diese Fragen helfen dabei, die Güte der Daten einzuschätzen.

# +
# 1.C
# -

# [XML](https://de.wikipedia.org/wiki/Extensible_Markup_Language) ist eine Auszeichnungssprache zur Darstellung hierarchischer Daten (wie z.B. Text, der in Kapiteln, Sätzen und Wörtern organisiert und mit Kommentaren und anderen Auszeichnungen versehen ist). [TEI-XML](https://de.wikipedia.org/wiki/Text_Encoding_Initiative) setzt auf XML auf und legt einen Standard zur Beschreibung von Text fest. TEI-XML ist im Bereich der Textverarbeitung sehr weit verbreitet.

# +
# 1.D

# um XML-Dateien lesen zu können, benötigen wir einen sog. "Parser", der
# das spezielle Format versteht. Das erledigt die Bibliothek "tei_reader" für
# uns, die auf TEI-XML-Dateien spezialisiert sind.
import tei_reader
# os gibt Zugriff auf die Funktionen des Betriebssystems
import os

# das ist der Pfad zu dem Verzeichnis, im dem alle koptischen Texte liegen
path_copt = "daten/sahidica.mark_TEI/koptisch"

# mit der Funktion "listdir()" lassen sich alle in einem Verzeichnis enthaltenen
# Dateien auflisten. Wir listen also 
filenames_copt = os.listdir(path_copt)

# da wir die Dateien gleich in der richtigen Reihe einlesen wollen, sortieren wir
# diese Liste noch
filenames_copt.sort()

# hier erzeugen wir ein "TeiReader" Objekt, das die Funktion "read_file()"
# besitzt. Diesem Objekt können wir Pfade zu TEI-XML-Dateien geben, die
# es dann für uns einliest und uns Zugriff auf den enthaltenen Text gibt
reader = tei_reader.TeiReader()

# dies ist der gesamte Pfad zur ersten XML-Datei im Verzeichnis
path_to_file = os.path.join(path_copt, filenames_copt[0])

# lies mit Hilfe der read_file()-Funktion die XML-Datei ein
corpus = reader.read_file(path_to_file)

# das Attribut "text" der Variable "corpus" beinhaltet 
# den koptischen Text, der in der XML-Datei gespeichert ist
text_copt = corpus.text

# zeige die ersten 100 Zeichen des Textes an
print(text_copt[0:100])

# +
# 1.E
# erzeuge wieder einen "reader"
reader = tei_reader.TeiReader()

# in dieser Liste werden wir die koptischen Kapitel speichern
chapters_copt = []

# jetzt iterieren wir über alle Dateinamen (Kapitel) im koptischen 
# Unterverzeichnis und lesen jede der 16 Dateien ein
for fname in filenames_copt:
    # dies ist der gesamte Pfad zur jeweiligen XML-Datei
    path_to_file = os.path.join(path_copt, fname)
    # lies mit Hilfe der read_file()-Funktion die XML-Datei ein
    corpus = reader.read_file(path_to_file)
    # das Attribut "text" der Variable "corpus" beinhaltet 
    # den koptischen Text, der in der XML-Datei gespeichert ist
    single_text = corpus.text
    # wir fügen den Text des geladenen Kapitels der Variablen hinzu,
    # in der wir den gesamten koptischen Text des Markus-Evangeliums
    # speichern
    chapters_copt.append(single_text)
    
# importiere pandas und erzeuge ein DataFrame mit den Kapiteln
import pandas as pd
chapters_copt = pd.DataFrame({'chapter':chapters_copt})

# zähle die Anzahl der Zeichen je Kapitel und füge sie in einer
# neuen Spalte dem DataFrame hinzu
char_count = [len(chap) for chap in chapters_copt['chapter']]
chapters_copt['char_count'] = char_count
chapters_copt.head()

# +
# 1.E Fortsetzung
# die Funktion iterrows() eines DataFrames liefert sowohl den Index als auch
# die in der Zeile enthaltenen Daten zurück
for i, row in chapters_copt.iterrows():
    zeichen =row['char_count']
    print("Kapitel {} hat {} Zeichen".format(i+1, zeichen))

gesamtanzahl = chapters_copt['char_count'].sum()
print('Das Markusevangelium hat {} Zeichen'.format(gesamtanzahl))

# +
# 1.F
# das ist der Pfad zu dem Verzeichnis, im dem alle englischen Texte liegen
path_eng = "daten/sahidica.mark_TEI/englisch"

# alle Dateien im Verzeichnis auflisten und sortieren
filenames_eng = os.listdir(path_eng)
filenames_eng.sort()

# dies ist der gesamte Pfad zur ersten XML-Datei im Verzeichnis
path_to_file = os.path.join(path_eng, filenames_eng[0])

# wir erzeugen ein sog. "file-handle", also einen "Weg zur Datei"
# mit der Funktion open()
file_handle = open(path_to_file)

# wir lesen den Text mit der Funktion "read()" ein
text_eng = file_handle.read()

# der Englische text ist in einzelne Sätze strukturiert, die immer
# mit einer Zahl (laufender Index) beginnen und mit einem Zeilenumbruch
# enden. Zwischen den einzelnen Sätzen befindet sich eine Leerzeile.
# Diese zusätzlichen Zeichen werden wir beachten müssen, wenn wir den
# Text später automatisiert analysieren.
print(text_eng)

# +
# 1.G

# in dieser Liste werden wir die englischen Kapitel speichern
chapters_eng = []

# jetzt iterieren wir über alle Dateinamen (Kapitel) im englischen 
# Unterverzeichnis und lesen jede der 16 Dateien ein
for fname in filenames_eng:
    # dies ist der gesamte Pfad zur jeweiligen Text-Datei
    path_to_file = os.path.join(path_eng, fname)
    # die Funktion open() gibt ein "handle" der Datei zurück, 
    # mit dem wir auf die Datei zugreifen können
    file_handle = open(path_to_file)
    # lies mit Hilfe der read()-Funktion die XML-Datei ein
    single_text = file_handle.read()
    # wir fügen den Text des geladenen Kapitels der Liste hinzu,
    # in dem wir alle Texte der einzelnen Kapitel speichern
    chapters_eng.append(single_text)

# erzeuge ein DataFrame aus der Liste
chapters_eng = pd.DataFrame({'chapter':chapters_eng})

# zähle die Anzahl der Zeichen je Kapitel und füge sie in einer
# neuen Spalte dem DataFrame hinzu
char_count = [len(chap) for chap in chapters_eng['chapter']]
chapters_eng['char_count'] = char_count
chapters_eng.head()

# +
# 1.H
# und alles nochmal für die Deutschen Texte

path_deu = "daten/sahidica.mark_TEI/deutsch"
filenames_deu = os.listdir(path_deu)
filenames_deu.sort()

chapters_deu = []
for fname in filenames_deu:
    path_to_file = os.path.join(path_deu, fname)
    file_handle = open(path_to_file)
    single_text = file_handle.read()
    chapters_deu.append(single_text)

chapters_deu = pd.DataFrame({'chapter':chapters_deu})
char_count = [len(chap) for chap in chapters_deu['chapter']]
chapters_deu['char_count'] = char_count
# gleich dem englischen Text ist der Deutsche in Sätze gegliedert,
# die immer mit einem laufenden Index eingeleitet werden.
chapters_deu.head()
# -

# <a name="unicode"></a> 2. Unicode
# ---

# [Anfang](#top)

# +
# 2.A
# -

# Unicode codiert Symbole digital, indem es die verschiedenen Symbole "durchnummeriert", dadurch wird es möglich, eine große Vielfalt von Symbolen zu codieren. Die große Anzahl an möglichen Zeichen ist hilfreich, um die vielfalt der Symbole darzustellen, die verschiedene Kulturen und verschiedene Schriften verwenden. Auch Emojis werden in Unicode codiert.  
# Für jede Nummer ist festgelegt, welches Symbol sie darstellt und diese Nummern werden dann von darstellenden Programmen (wie z.B. das Jupyter-Notebook oder WhatsApp) zurück in die entsprechenden Symbole übersetzt. In Unicode können 17 "Ebenen" codiert werden - Im Endeffekt entspricht jede Ebene einer "Stelle" in der Nummerierung. Ein Zeichen auf Ebene 17 würde also mit 17 Zahlen codiert werden. Im aktuellen Unicode-Standard werden 6 Ebenen verwendet und es können 137.929 Zeichen codiert werden. Die aktuell verwendeten Ebenen sind in "Blöcke" unterteilt, in denen sich jeweils Zeichen verschiedener schriften finden:  
#
# ![Bild](https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Roadmap_to_Unicode_BMP_multilingual.svg/langde-1920px-Roadmap_to_Unicode_BMP_multilingual.svg.png)
#
#
# Die von uns verwendeten koptischen Symbole liegen im Block für "andere europäische Schriften" (erste Zeile, blau). Das ist dadurch erkennbar, dass unsere koptischen Symbole alle mit ```u02``` oder ```u03``` beginnen, wobei das ```u``` für Unicode steht, während die Zahl den Block bezeichnet.

# +
# 1.B. 
# -

# Die Symbole ```"\n"``` (Zeilenumbruch), ```"."```, ```"["``` und ```"]"``` sowie das Leerzeichen gehören nicht zum Koptischen Alphabet. Im ersten Kapitel kommen 29 koptische Schriftzeichen vor, das koptische Alphabet hat aber 30 Schriftzeichen. Das Zeichen, das im ersten Kapitel nicht vorkommt, ist ```\u2caf``` bzw.

print('\u2caf')

# +
# Exkurs (absolut freiwillig)
# einfach herausfinden, welches Symbol im ersten Kapitel nicht
# vorkommt lässt sich durch einen Vergleich mit den Symbolen
# im dritten Kapitel. Dafür können wir uns die Mengen-Operation
# der Differenz zunutze machen. Siehe Dokumentation von Mengen
# in Python hier: https://docs.python.org/2/library/sets.html

# erstelle die Menge der Symbole im ersten Kapitel
chapter01_symbols = set(chapters_copt.loc[0]['chapter'])
# erstelle die Menge der Symbole im dritten Kapitel
chapter03_symbols = set(chapters_copt.loc[2]['chapter'])
# erstelle die Differenz der Menge (das Resultat ist selbst eine Menge)
difference = chapter03_symbols.difference(chapter01_symbols)
# verwandle die Menge in eine Liste
difference = list(difference)
# gib die Liste aus
print(difference)
print(difference[0].encode('raw_unicode_escape'))

# +
# 2.C
vorname = 'Jana'
print(vorname.lower())

nachname = 'lasser'
print(nachname.upper())

testsymbol = '\u2ca3'
print(testsymbol)

print(testsymbol.upper())
print(testsymbol.upper().encode('raw_unicode_escape'))
# der entsprechende Großbuchstabe liegt anscheinend im 
# Unicode-Block direkt vor dem Kleinbuchstaben. Bei
# der Überführung in Groschreibung wird also die Unicode-
# Nummer einfach um 1 verringert.

# +
# 1.D
# Kapitel 3 enthält alle 30 Schriftzeichen
symbols = set(chapters_copt.loc[2]['chapter'])
# eine Liste der nicht-koptischen Zeichen, die bisher so vorgekommen sind
non_coptic = ['\n', ' ', '.', ',', ';', '[', ']']
# entferne die nicht-koptischen Zeichen
symbols = [sym for sym in symbols if sym not in non_coptic]

# erstelle eine Tabelle mit den koptischen Symbolen und ihrer
# Unicode-Codierung
symbol_table = pd.DataFrame()
symbol_table['symbol'] = symbols
symbol_table['unicode'] = [sym.encode('raw_unicode_escape') for\
                    sym in symbols]

# zeige die Tabelle an
symbol_table.head(50)
# -

# [Anfang](#top)

# <a name="histogramme"></a> 3. Histogramme
# ---

# +
# 3.A
# importiere das Untermodul pyplot aus der Bibliothek
# matplotlib zum darstellen von Grafiken
import matplotlib.pyplot as plt
# mit diesem Kommando teilen wir dem Jupyter-Notebook mit,
# dass es Grafiken direkt im Notebook anzeigen soll
# %matplotlib inline

# erstelle ein einfaches Histogram der Kapitellängen
plt.hist(chapters_copt['char_count']);

# +
# 3.B
# die Werte liegen alle in einem Bereich von 1000 bis 9000 Zeichen
# indem wir die "range" entsprechend festlegen und die Anzahl der Bins
# auf 8 setzen, liegen die einzelnen Bins zwischen "glatten" Vielfachen
# von 1000. Das ist anschaulich in einfach zu interpretieren. Diese 
# Darstellung lässt z.B. direkt ablesen, dass 6 Kapitel des koptischen
# Markus-Evangeliums zwischen 3000 und 4000 Zeichen haben.

# die rwidth auf einen Wert kleiner 1.0 zu setzen ist auch eine gute Idee,
# da es die einzelen bins in der Darstellung besser voneinander abgrenzt.
plt.hist(chapters_copt['char_count'], range=[1000,9000], bins=8, rwidth=0.8)
plt.xlabel('Kapitellänge / Zeichen')
plt.ylabel('Anzahl')
plt.title('Verteilung der Kapitellängen im Markusevangelium');
# -

# 3.C
# wenn eine (zu) kleine Anzahl bins gewählt wird, geht viel Information
# verloren. 
plt.hist(chapters_copt['char_count'], range=[1000,9000], bins=2, rwidth=0.8)
plt.xlabel('Kapitellänge / Zeichen')
plt.ylabel('Anzahl')
plt.title('Verteilung der Kapitellängen im Markusevangelium');

# bei einer zu großen Anzahl von bins liegt irgendwann praktisch jede Beobachtung
# (Kapitellänge) in ihrem eigenen Bin und es wird schwer, Information über die 
# Menge der Beobachtungen zu aggregieren
plt.hist(chapters_copt['char_count'], range=[1000,9000], bins=100, rwidth=0.8)
plt.xlabel('Kapitellänge / Zeichen')
plt.ylabel('Anzahl')
plt.title('Verteilung der Kapitellängen im Markusevangelium');
# eine gute Wahl für den Datensatz scheint bins=8 (wie in 1.B) zu sein

# +
# 3.D
plt.hist(chapters_copt['char_count'], range=[1000,9000], bins=8, rwidth=0.8, alpha=0.5, label='koptisch')
plt.hist(chapters_eng['char_count'], range=[1000,9000], bins=8, rwidth=0.8, alpha=0.5, label='englisch')
#plt.hist(chapters_deu['char_count'], range=[1000,9000], bins=8, rwidth=0.8, alpha=0.5, label='deutsch')
plt.xlabel('Kapitellänge / Zeichen')
plt.ylabel('Anzahl')
plt.title('Verteilung der Kapitellängen im Markusevangelium')
plt.legend()

# die englischen Kapitel scheinen tendentiell etwas länger zu sein als die koptischen
# damit die Darstellung vergleichbar ist, muss das Argument range und bins jeweils gleich 
# gewählt sein
# -

# 3.E (density)
# das Argument "density" verändert die Darstellung der Beobachtungen auf der y-Achse. 
# Vorher war die absolute Anzahl der Beobachtungen abgebildet, mit density=True
# wird die normierte Anzahl der beobachtungen abgebildet, die als Wahrscheinlichkeits-
# dichte interpretiert werden kann
plt.hist(chapters_copt['char_count'], range=[1000,9000], bins=8, rwidth=0.8, alpha=0.5, label='koptisch', density=True)
plt.hist(chapters_eng['char_count'], range=[1000,9000], bins=8, rwidth=0.8, alpha=0.5, label='englisch', density=True)
plt.xlabel('Kapitellänge / Zeichen')
plt.ylabel('Wahrscheinlichkeitsdichte')
plt.title('Wahrscheinlichkeitsdichte der Kapitellängen im Markusevangelium')
plt.legend()

# 3.E (cumulative)
# wenn cumulative=True, dann zeigt jeder bin nicht nur die Anzahl der Beobachtungen
# in ebendiesem bin sondern die Anzahl in dem bin PLUS die Anzahl der Beobachtungen
# in allen Vorangegangenen bins. Die Anzahl der Beobachtungen im letzten Bin entspricht
# also immer der Gesamtanzahl der Beobachtungen.
plt.hist(chapters_copt['char_count'], range=[1000,9000], bins=8, rwidth=0.8, alpha=0.5, label='koptisch', cumulative=True)
plt.hist(chapters_eng['char_count'], range=[1000,9000], bins=8, rwidth=0.8, alpha=0.5, label='englisch', cumulative=True)
plt.xlabel('Kapitellänge / Zeichen')
plt.ylabel('Wahrscheinlichkeitsdichte')
plt.title('Kumulative Verteilung der Wahrscheinlichkeitsdichten der Kapitellängen im Markusevangelium')
plt.legend()

# 3.E (density & cumulative)
# ist sowohl density=True als auch cumulative=True, dann lässt sich schön beobachten,
# dass die Wahrscheinlichkeitsdichte auf 1 normiert ist, dass heißt die kumulierten 
# Wahrscheinlichkeiten, eine Beobachtung in einem Bin zu finden ist im letzten bin gleich 1
plt.hist(chapters_copt['char_count'], range=[1000,9000], bins=8, rwidth=0.8, alpha=0.5, label='koptisch', cumulative=True, density=True)
plt.hist(chapters_eng['char_count'], range=[1000,9000], bins=8, rwidth=0.8, alpha=0.5, label='englisch', cumulative=True, density=True)
plt.xlabel('Kapitellänge / Zeichen')
plt.ylabel('Anzahl')
plt.title('Kumulative Verteilung der Kapitellängen im Markusevangelium')
plt.legend()

# [Anfang](#top)
