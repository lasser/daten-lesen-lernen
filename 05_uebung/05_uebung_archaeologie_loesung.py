# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _Lizenz: Das folgende Lehrmaterial kann unter einer [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) Lizenz frei verwendet, verbreitet und modifiziert werden._   
#  _Authoren: Jana Lasser (jana.lasser@ds.mpg.de)_  
#  _Das Lehrmaterial wurde im Zuge des Projektes "Daten Lesen Lernen", gefördert vom Stifterverband und der Heinz Nixdorf Stiftung erstellt._

# # Lösung zu Übung 05 - Archäologie: Daten finden, einlesen und mit Histogrammen visualisieren
# ### Praktische Hinweise
# $\rightarrow$ Übungen sind dafür gedacht, dass ihr sie euch daheim anseht und versucht, ein paar Aufgaben selbst zu lösen. In den Tutorien könnt ihr euch weiter mit dem Lösungen der Aufgaben beschäftigen und dabei Hilfe von den Tutor*innen bekommen.  
#
# $\rightarrow$ Wenn ein Fehler auftritt:
# 1. Fehlermeldung _lesen_ und _verstehen_
# 2. Versuchen, selbst eine Lösung zu finden ($\rightarrow$ hier findet das Lernen statt!)
# 3. Das Problem googlen (Stichwort: Stackoverflow) oder den/die Nachbar*in fragen
# 4. Tutor*in fragen
#
# $\rightarrow$ Unter <font color='green'>**HINWEIS**</font> werden Hinweise gegeben, die für die Lösung der Aufgabe hilfreich sind und oftmals auch weiterführende Informationen zur Aufgabe liefern.
#
# $\rightarrow$ Mit **(Optional)** gekennzeichnete Übungsteile sind für besonders schnelle Menschen :-).

# <a name="top"></a>Inhalt
# ---
#  Mit dieser Übung beginnen wir eine Fallstudie, die sich mit einem Datensatz aus der Archäologie beschäftigt: dem ["Inventory of Crafts and Trade in the Roman East - database of tableware" (ICRATES)](https://archaeologydataservice.ac.uk/archives/view/icrates_lt_2018/downloads.cfm). Außerdem werden wir uns damit beschäftigen, wie wir die gewonnenen Informationen am besten visualisieren können. Die aktuelle Übung gliedert sich in drei Teile:
# * [Daten beschaffen](#daten_beschaffen)
# * [Daten erkunden & bereinigen](#daten_erkunden)
# * [Histogramme](#histogramme)  
#
# Für die folgenden Übungen gibt es kein Lehrvideo. Deswegen wird es in den Jupyter-Notebooks zu den Übungen mehr Erklärungen und Zwischenschritte geben. Darüber hinaus haben ab dieser Woche auch nicht mehr alle Übungen der verschiedenen Übungsgruppen den exakt gleichen Inhalt, da einige Themenbereiche etwas unterschiedliche Werkzeuge brauchen als andere. Das Konzept ist aber überall das gleiche.

# <a name="daten_beschaffen"></a>1. Daten beschaffen
# ---
# **A.** Mache dich mit der Datenquelle und ihrem Aufbau vertraut: Wer hat die Daten gesammelt und in welchem Kontext publiziert? Was für Informationen finden sich im ```ICRATES_CATALOGUE``` und wie sind sie mit den anderen Tabellen wie ```ICRATES_LOCATION``` verknüpft?  
#
# Wir können den Datensatz direkt aus dem Internet herunterladen:

# +
# Bibliotheken zum Laden von Daten aus dem Internet
import requests 
import io

# die URL unter der der Datensatz zu finden ist
pfad_database = "https://archaeologydataservice.ac.uk/catalogue/adsdata/arch-3268-1/dissemination/csv/"

# +

# lies die Information von der in der URL hinterlegten Website aus
# Der Dateiname der'ICRATES_METADATA.csv' liegt im Ordner 'documentation' (Steht in der URL ganz am Ende)

antwort_metadata = requests.get(pfad_database + 'documentation/'+'ICRATES_METADATA.csv').content

# Ein kurzer Blick in die Metadata Datei gibt uns an, wann und wie der gesamte Datensatz publiziert wurde
# und wie verschiedenen Dateien miteinander Verknuepft sind:
# diesmal geben wir das encoding schon an dieser Stelle an

print(antwort_metadata.decode('ISO-8859-1'))
# -

# Der Dateiname der wichtigsten (und groessten) Datei, des Katalogs:
name_catalogue = "ICRATES_CATALOGUE.csv"
antwort_catalogue = requests.get(pfad_database + name_catalogue).content
inhalt_catalogue = io.StringIO(antwort_catalogue.decode('ISO-8859-1'))

# lade die Daten in ein DataFrame und zeige die ersten paar Zeilen an
import pandas as pd
catalogue = pd.read_csv(inhalt_catalogue)
catalogue.head()

# **B.** Lade auch die anderen zur Datenbasis gehörenden Tabellen wie oben (oder von Hand) herunter.  
# <font color='green'>**HINWEIS:** Du kannst die Tabellen mit der funktion ```DataFrame.to_csv()``` lokal speichern (Funktionsweise siehe [Dokumentation](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_csv.html) ).</font>

catalogue.to_csv("../daten/" + name_catalogue)

# +
# Namen aller anderen verfuegbaren Dateien:

name_DEPOSIT       = "ICRATES_DEPOSIT"
name_LOCATION      = "ICRATES_LOCATION"
name_LRP           = "ICRATES_LRP"
name_OCK           = "ICRATES_OCK"
name_PUBLICATION   = "ICRATES_PUBLICATION"
name_STANDARD_FORM = "ICRATES_STANDARD_FORM"

# in einer Liste:
namen = [  
   name_DEPOSIT,   
   name_LOCATION,   
   name_LRP,   
   name_OCK,   
   name_PUBLICATION,   
   name_STANDARD_FORM 
]

# +
# Wir laden alle Dateien in ein 'Dictionary'
# Dictionaries sind aehnlich wie Listen, aber die Eintraege sind aufrufbar mit einem Namen
inhalte = {}

# Somit sparen wir uns das Herunterladen der Dateien einzeln:
for name in namen:
    print('Lade Datei {} herunter.'.format(name))
    antwort_tmp = requests.get(pfad_database + name+'.csv').content
    inhalt_tmp = io.StringIO(antwort_tmp.decode('ISO-8859-1'))
    inhalte[name] = pd.read_csv(inhalt_tmp)
# -

# Auf die einzelnen DataFrames in 'inhalte' koennen wir ueber die 'namen' zugreifen
# Zeige die ersten 10 Einträge an
inhalte['ICRATES_DEPOSIT'][0:10]

# Speichern aller Datein lokal als .csv
for name, inhalt in inhalte.items():
    inhalt.to_csv("../daten/ICRATES" + name+'.csv')

# So koennen wir die Dateien auch ohne Internetverbindung laden:
inhalte = {name: pd.read_csv("../daten/ICRATES" + name + '.csv') for name in namen}

# [Anfang](#top)

# <a name="daten_erkunden"></a>2. Daten erkunden & bereinigen
# ---
# Zu Beginn möchten wir ein paar grundlegende Dinge über den Datensatz herausfinden: z.B. die enthaltene Information in den Spalten und die Anzahl der Einträge.
# Die Namen der Spalten eines DataFrame lassen sich über die Variable ```columns``` des DataFrames ansehen:

# eine Liste der im DataFrame "catalogue" enthaltenen Spalten
catalogue.columns

# In einem DataFrame können wir auf eine bestimmte Spalte mit ihrem Namen zugreifen:

# +
# zeigt die Spalte mit dem Namen "Location_specific" im DataFrame 
# "catalogue" an
catalogue.Location_specific 

# auch moeglich:
catalogue['Location_specific']
# -

# Mit der Funktion ```unique()``` lassen sich die einzigartigen Einträge in einer Spalte herausfinden:

# eine Liste der Fundorte, wobei jeder Fundort genau einmal
# vorkommt
catalogue.Location_specific.unique()

# [Anfang](#top)

# ### Aufgaben
# **A.** Wie heißen die Spalten in den Datensätzen ```ICRATES_LOCATION```, ```ICRATES_DEPOSIT``` und ```ICRATES_STANDARD_FORM``` und welche Informationen enthalten sie?  
# **B.** Sieh dir ein paar Einträge aus dem Katalog an, indem du auf den Index zugreifst.  
# **C.** Wieviele Fundstücke und Fundorte enthält der Katalog? Was ist die mittlere Anzahl an Fundstücken je Fundort?  
# **D.** Die Spalte ```Min_Rim_Diameter_mm``` enthält eine Abschätzung des Umfanges der Tongefäße am oberen Rand. Berechne den mittleren Umfang und den Median des Umfangs. Berechne auch den Modus des Umfangs. Wie kannst du dir das Ergebnis erklären?  
# **E.** Manche Spalten enthalten nur sporadisch Einträge und sind sonst leer. Fehlende Einträge werden im ```DataFrame``` als ```NaN``` ([not a number](https://de.wikipedia.org/wiki/NaN)) dargestellt. Spalten wie ```Gouged``` oder ```Glazed``` enthalten Nullen und Einsen. Wie sind diese zu interpretieren? 
#
# Die Anzahl der ```NaN```-Einträge lässt sich mit der Funktion ```isna()``` herausfinden:

# **A.** Wie heißen die Spalten in den Datensätzen ```ICRATES_LOCATION```, ```ICRATES_DEPOSIT``` und ```ICRATES_STANDARD_FORM``` und welche Informationen enthalten sie?  
#

inhalte['ICRATES_LOCATION'].columns

# Wir greifen hier beispielsweise auf die Spalten in der Datenbank zu, die fü
# r die Zeilen 11 bis 13 die Namen der Fundorte zu unterschiedlichen Zeiten anzeigen.

inhalte['ICRATES_LOCATION'][ ['Early_Imperial_Province', 
                              'Early_Byzantine_region',
                              'Middle_Imperial_Province', 
                              'Modern_country'] 
                           ].loc[11:13]

# In den Spalten der Datensaetze findet man weitergehende Informationen zu den Orten der Fundstücke, wie zum Beispiel welche Flüsse oder Meere in der Nähe liegen, oder wie der Fundort zu unterschiedlichen Zeitraeumen benannt war.

# +

inhalte['ICRATES_DEPOSIT'].columns
# -

inhalte['ICRATES_STANDARD_FORM'].columns

inhalte['ICRATES_STANDARD_FORM'].Fabric

# Die Spalte "Fabric" zeigt beispielsweise das Herstellungsverfahrung an, das fuer einen bestimmten Ort typisch war und dadurch eventuell Rueckschuesse auf den Zeitraum liefert, zu dem die Technik dort angewendet wurde.

inhalte['ICRATES_STANDARD_FORM'].Standard_Form.unique()

# Die Spalte "Standard_Form" zeigt vermutlich Abkuerzungen fuer bestimmte Formtypen der Keramik an.

# **B.** Sieh dir ein paar Einträge aus dem Katalog an, indem du auf den Index zugreifst.  

# Wir schauen uns einfach mal die Eintraege 100 bis 110 an. 

catalogue.loc[100:110]

# **C.** Wieviele Fundstücke und Fundorte enthält der Katalog? Was ist die mittlere Anzahl an Fundstücken je Fundort?  
#

# Die Anzahl der Fundstuecke bekommen wir, indem wir zaehlen, wie viele verschiedene ```ICRATES_ID```s es gibt:

catalogue.ICRATES_ID.unique()

# ```.unique()``` gibt einen numpy array zurück (für unsere Zwecke verhält sich das gleich wie eine Liste), wenn wir die Länge dieses arrays wissen wollen, schauen wir uns mit ```len``` die Grösse dessen an.

len(catalogue.ICRATES_ID.unique())

# Das gleiche fuer die Fundorte:

len(catalogue.Location_ID.unique())

# **D.** Die Spalte ```Min_Rim_Diameter_mm``` enthält eine Abschätzung des Umfanges der Tongefäße am oberen Rand. Berechne den mittleren Umfang und den Median des Umfangs. Berechne auch den Modus des Umfangs. Wie kannst du dir das Ergebnis erklären?  

catalogue.Min_Rim_Diameter_mm.mean()

catalogue.Min_Rim_Diameter_mm.median()

catalogue.Min_Rim_Diameter_mm.mode()

# Der Modus ist ```0.0```. Diese offensichtlich keinen Durchmesser darstellende Zahl taucht am oeftesten auf,  Sie koennte verwendet worden sein, wenn man keine Rueckschuesse ueber den Randdurchmesser der Fundstuecks ziehen konnte. Diese eintraege verzerren die Statistik, sie ziehen Mittelwert und Median zu kleineren Zahlen.

# **E.** Manche Spalten enthalten nur sporadisch Einträge und sind sonst leer. Fehlende Einträge werden im ```DataFrame``` als ```NaN``` ([not a number](https://de.wikipedia.org/wiki/NaN)) dargestellt.
#
# Die Anzahl der ```NaN```-Einträge lässt sich mit der Funktion ```isna()``` herausfinden:

# in der Spalte "Min_Rim_Diameter_mm" des Katalogs z.B.
# gibt es 23137 fehlende Einträge
catalogue.Min_Rim_Diameter_mm.isna().sum()

catalogue.Min_Base_Diameter_mm.isna().sum()

# Spalten wie ```Gouged``` oder ```Glazed``` enthalten Nullen und Einsen. Wie sind diese zu interpretieren? 

# Mit ```0``` wird nein codiert, ```1``` steht fuer ja, dies ist die speicherfreundlichste Art ja/nein Informationen anzugeben.

# **F. (Optional)** Die Nullen in der Spalte ```Min_Rim_Diameter_mm``` sind offensichtlich als "fehlender Eintrag" gemeint. Ersetze alle Nullen in dieser Spalte durch ```NaN```. Speichere die bereinigte Datei mit der Funktion ```catalogue.to_csv()``` ab. Wie verändert sich der Mittelwert, Median und Modus der Spalte?  
# <font color='green'>**HINWEIS:** dafür musst du das "NaN-Objekt" das von der Bibliothek ```NumPy``` bereit gestellt wird benutzen:</font> 

import numpy as np
np.nan

# Zuerst suchen wir alle Werte raus, die identisch 0 sind
hier_gleich_0 = catalogue.Min_Rim_Diameter_mm == 0

# ein kurzer Check, wir lassen uns hiermit nur die Werte rausgeben, die =0 sind:
catalogue.Min_Rim_Diameter_mm[hier_gleich_0]

# Bevor wir an den Daten arbeiten, lieber eine Sicherheitskopie machen
# cat = catalogue.copy()

# Erste Idee: alle Werte gleich NaN setzen:
cat.Min_Rim_Diameter_mm[hier_gleich_0] = np.NaN

# +
# dies ^ scheint eine Warnung hervorzurufen, pandas weist uns darauf hin,  
# dass der Wert NaN hier einer Kopie des Ausschnitts von 'cat' zugewiesen werden soll. 
# Manchmal kann das wohl zu Fehlern führen, die in der Dokumentation empfohlene Methode
# ist die Folgende:

cat.loc[hier_gleich_0,('Min_Rim_Diameter_mm')] = np.NaN
# -

cat.to_csv("../daten/ICRATES/ICRATES_CATALOGUE.csv")

cat.Min_Rim_Diameter_mm.mean()

cat.Min_Rim_Diameter_mm.median()

cat.Min_Rim_Diameter_mm.mode()

# Offensichtlich hat das entfernen aller ```0.0``` Einträge die Statistik verändert. Der Modus ist nun nicht mehr ```0.0``` und auch Mittelwert und Median liegen bei höheren (vermutlich korrekten) Werten.

#   **G.** Finde für jede Spalte des Katalogs heraus, wieviele (fehlende) Einträge es in der Spalte gibt. Erstelle ein neues ```DataFrame```, das jeweils den Spaltennamen und die Anzahl der Einträge enthält.  

# Die Summe lauft automatisch uber alle Spalten, auch wenn wir 
# vorher eine andere Funktion auf das DataFrame anwenden
catalogue.isna().sum()

# **H.** Welche Spalten haben keine fehlenden Einträge? In welchen Spalten ist am wenigsten Information enthalten?  

# Wenn die Summe der fehlenden Eintraege gleich 0 ist, enthalten die Spalten keine fehlenden Eintraege:
catalogue.columns[catalogue.isna().sum() == 0]

# pandas hat die Funktion 'idxmax()', welche uns den Eintrag mit den maximalen Eintraegen zurueck gibt:
catalogue.isna().sum().idxmax()

# **I. (Optional)** Erstelle mit Hilfe der Bibliothek ```matplotlib.pyplot``` ein sog. "bar-chart", in dem die Anzahl der fehlenden Einträge je Spalte visualisiert sind.  
# <font color='green'>**HINWEIS:** [Hier](https://pythonspot.com/matplotlib-bar-chart/) findest du ein Beispiel für ein bar-chart und [Hier](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.bar.html) ist die Funktion ```bar()``` dokumentiert. </font>

import matplotlib.pyplot as plt

# +
# Für einen bar plot brauchen wir eine Nummerierung der einzelnen Spalten:
columns_id = list(range(len(catalogue.columns)))

# Die Angaben von oben packen wir auch in eine Variable:
columns_num_of_nan = catalogue.isna().sum()
# -

plt.bar(columns_id, columns_num_of_nan)
plt.show()

# ein bisschen schoener und aufschlussreicher:
fig, ax = plt.subplots(1,1, figsize=(25,5))
ax.bar(columns_id, columns_num_of_nan)
ax.set_xticks(columns_id)
ax.set_xticklabels(catalogue.columns, rotation=90, fontsize=10)
plt.show()

# [Anfang](#top)

# <a name="histogramme"></a>4. Histogramme
# ---
# Im folgenden Abschnitt werden wir damit beginnen, die in den Daten enthaltenen Informationen zu visualisieren. Eine sehr einfache Visualisierung ist das Histogramm, eine graphische Darstellung der Häufigkeit eines Merkmals. Zur Illustration erstellen wir eine kleine Visualisierung der Verteilung von Werten in einer Liste:

# +
# eine Liste mit 30 Ganzzahlen, die z.B. das Alter von Personen in einer
# Gruppe darstellen können
alter = [37, 20, 84,  2, 11, 89, 52, 65, 90, 21, 30, 12, 17, 53, 62, 45, 37,
        3,  6, 64, 85,  6, 95, 73, 68, 55, 86, 83, 91,  1]

# für Visualisierungen benutzen wir die Blbiothek "matplotlib.pyplot"
import matplotlib.pyplot as plt

# Histogramme lassen sich einfach mit der Funktion plt.hist() erstellen
plt.hist(alter);
# -

# Visualisierungen sollten immer mit Achsenbeschriftungen versehen werden. Dafür verwenden wir die Funktionen ```plt.xlabel()``` und ```plt.ylabel```. Außerdem können wir der Abbildung mit ```plt.title()``` einen Titel geben. Mit verschiedenen optionalen Argumenten lässt sich das Verhalten von ```plt.hist()``` anpassen:
# * ```bins``` ändert die Anzahl der Bins
# * ```range``` ändert den dargestellten Bereich auf der x-Achse
# * ```rwidth``` ändert die dargestellte bin-Weite 
# * ```color``` ändert die Farbe
# * ```alpha``` ändert den Alpha-Wert (Durchsichtigkeit) der dargestellten Balken.
#
# Plot-Funktionen haben oftmals sehr viele optionale Argumente (auch "keyword arguments" genannt). Um herauszufinden, wass die jeweiligen Funktionen alles können, lohnt sich ein Blick in die [Dokumentation](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.hist.html). Außerdem sollte jede Darstellung *immer* mit Achsenbeschriftungen und eventuell einem Titel versehen werden:

plt.hist(alter, color='red', rwidth=0.8, range=[0,100], bins=10, label='alter')
plt.xlabel('Alter')
plt.ylabel('Anzahl')
plt.legend()
plt.title('Verteilung des Alters');

# [Anfang](#top)

# ### Aufgaben
# **A.** Erstelle ein Histogramm der in der Spalte ```Min_Rim_Diameter_mm``` enthaltenen Durchmesser. <font color='green'>**HINWEIS:** Damit das funktioniert, musst du mit der Funktion ```dropna()``` alle ```NaN```-Werte entfernen.</font>  

plt.hist(cat.Min_Rim_Diameter_mm.dropna() )
plt.show()

# **B.** Mache dich mit den unterschiedlichen Argumenten der Funktion ```plt.hist()``` vertraut, indem du verschiedene Werte und Kombinationen für die Daten ausprobierst. Finde Werte, die die Daten deiner Meinung nach gut und verständlich darstellen. Worauf kommt es dabei an? Versieh die Darstellung mit passenden Beschriftungen.  

plt.figure(figsize=(15,5))
plt.hist(cat.Min_Rim_Diameter_mm.dropna(), color='darkred', rwidth=0.8, bins=25, label='ICRATES Catalogue')
plt.xlabel('Min_Rim_Diameter [mm]')
plt.ylabel('Counts')
plt.legend()
plt.title('Verteilung der Randdurchmesser');

plt.hist(cat.Min_Rim_Diameter_mm.dropna(), bins=100)
plt.show()

plt.hist(cat.Min_Rim_Diameter_mm.dropna(), bins=100, histtype='step')
plt.show()

# **C.** Insbesondere das Argument ```bins``` verändert die Darstellung des Histogrammes sehr stark. Was kann schiefgehen, wenn ```bins``` zu niedrig oder zu hoch gewählt wird? Was ist eine gute Wahl für einen (diesen) Datensatz?  

# +
plt.hist(cat.Min_Rim_Diameter_mm.dropna(), 5)
plt.show()

plt.hist(cat.Min_Rim_Diameter_mm.dropna(), 25)
plt.show()

plt.hist(cat.Min_Rim_Diameter_mm.dropna(), 50)
plt.show()

plt.hist(cat.Min_Rim_Diameter_mm.dropna(), 100)
plt.show()

plt.hist(cat.Min_Rim_Diameter_mm.dropna(), 250)
plt.show()

plt.hist(cat.Min_Rim_Diameter_mm.dropna(), 2500)
plt.show()
# -

# Wählen wir die bin - Anzahl zu klein, verlieren wir Informationen, weil wir zu viele unterschiedliche Informationen zusammen fassen. Bei zu großen Werten im Gegensatz zeigen wir mehr oder weniger die einzelnen Werte für die Durchmesser direkt an. Beides erlaubt uns nicht, einen Trend in den Daten zu finden.
# Ein guter Wert, um einen Trend zu finden, ist hier zwischen 25 und 50, wir erkennen deutlich 2 Maxima in den Durchmessern.

# **D.** Visualisiere die Durchmesser an der Gefäßbasis (```Min_Base_Diameter_mm```) im selben Histogram. Was für Aussagen über die Gefäße kannst du aus dieser Visualisierung ableiten? <font color='green'>**HINWEIS:** Du kannst einfach in der selben Code-Zelle zwei mal hintereinander die Funktion ```plt.hist()``` (natürlich mit unterschiedlichen Daten) aufrufen, um die Daten übereinandergelegt darzustellen. Du kannst mit der Funktion ```plt.legend()``` eine Legende zur Abbildung hinzufügen. Damit das funktioniert, musst du den einzelen plot-Befehlen vorher ein entsprechendes ```label``` Argument mitgeben.</font> Was fällt dir auf? Worauf musst du achten, damit die in beiden Histogrammen dargestellten Werte vergleichbar sind? Warum ist diese Darstellung eventuell irreführend?  

plt.hist(cat.Min_Rim_Diameter_mm.dropna(), bins=40, range=[0,800], label='Rim')
plt.hist(cat.Min_Base_Diameter_mm.dropna(), bins=40, range=[0,800], label='Base')
plt.legend()
plt.show()

# * Damit die beiden Histogramme vergleichbar sind, bietet es sich an, jeweils die gleiche Anzahl an bins und den gleichen Bereich (```range=[min,manx]```) abzubilden. 
# * In dieser Darstellung sieht es so aus, als ob die Gefäße an der Basis im Schnitt deutlich kleiner sind als am oberen Rand. 
# * In dem Datensatz gibt es wesentlich weniger Einträge zu Basis-Durchbessern als zu Durchmessern vom oberen Rand. Das darsgestellte Ergebnis könnte als zum einen daran liegen, dass die Gefäße an der Basis tatsächlich kleiner sind. Eine alternative Erklärung wäre, dass eher Stücke von kleinen Gefäßen, aus denen sich der Basis-Durchmesser errechnen lässt, erhalten sind.

# **E. (Optional)** Was hat es mit den beiden keyword arguments ```density``` und ```cumulative``` auf sich? Wie verändert sich das Histogram und wie kann die Darstellung dann interpretiert werden?

# Die Dokumentation (```Shift + Tab```) zeigt uns folgendes:
#
# **density**:
# _the counts normalized to form a probability density, i.e., the area (or integral) under the histogram will sum to 1_
#     
# **cumulative**:
# _each bin gives the counts in that bin plus all bins for smaller values. The last bin gives the total number of datapoints_
#
#

plt.hist(cat.Min_Rim_Diameter_mm.dropna(), density=True)
plt.show()

plt.hist(cat.Min_Rim_Diameter_mm.dropna(), density=True, bins=100, cumulative=True)
plt.show()

plt.hist(cat.Min_Rim_Diameter_mm.dropna(), bins=40, range=[0,800], alpha=.5, density=True, label='Rim')
plt.hist(cat.Min_Base_Diameter_mm.dropna(), bins=40, range=[0,800], alpha=.5, density=True, label='Base')
plt.legend()
plt.show()

# [Anfang](#top)
