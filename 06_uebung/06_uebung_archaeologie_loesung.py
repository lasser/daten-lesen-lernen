# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _Lizenz: Das folgende Lehrmaterial kann unter einer [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) Lizenz frei verwendet, verbreitet und modifiziert werden._   
#  _Authoren: Jana Lasser (jana.lasser@ds.mpg.de), Max Vossel_  
#  _Das Lehrmaterial wurde im Zuge des Projektes "Daten Lesen Lernen", gefördert vom Stifterverband und der Heinz Nixdorf Stiftung erstellt._

# # Lösung Übung 06 - Archäologie: Histogramme, deskriptive Statistik und Zeitreihen
# ### Praktische Hinweise
# $\rightarrow$ Übungen sind dafür gedacht, dass ihr sie euch daheim anseht und versucht, ein paar Aufgaben selbst zu lösen. In den Tutorien könnt ihr euch weiter mit dem Lösungen der Aufgaben beschäftigen und dabei Hilfe von den Tutor*innen bekommen.  
#
# $\rightarrow$ Wenn ein Fehler auftritt:
# 1. Fehlermeldung _lesen_ und _verstehen_
# 2. Versuchen, selbst eine Lösung zu finden ($\rightarrow$ hier findet das Lernen statt!)
# 3. Das Problem googlen (Stichwort: Stackoverflow) oder den/die Nachbar*in fragen
# 4. Tutor*in fragen
#
# $\rightarrow$ Unter <font color='green'>**HINWEIS**</font> werden Hinweise gegeben, die für die Lösung der Aufgabe hilfreich sind und oftmals auch weiterführende Informationen zur Aufgabe liefern.
#
# $\rightarrow$ Mit **(Optional)** gekennzeichnete Übungsteile sind für besonders schnelle Menschen :-).

# <a name="top"></a>Inhalt
# ---
# In der folgenden Übung werden wir uns die Eigenschaften der Geschirr-Fundstücke etwas genauer ansehen. Insbesondere interessieren wir uns für die Größe des Geschirrs und sein Alter. Dafür benutzen wir neben der Haupttabelle, die den Katalog der Fundstücke enthält, auch eine zweite Tabelle aus dem Datensatz, die uns zusätzliche Informationen über das Alter von verschiedenen Geschirr-Typen liefert. Da die letzte Übung etwas lang war, wiederholen wir den Teil über die Histogramme aus der letzten Übung hier noch einmal. 
#
# Die aktuelle Übung gliedert sich in drei Teile:
# * [Histogramme](#histogramme)
# * [Größe des Geschirrs](#geschirrgroesse)
# * [Alter der Fundstücke](#fundstueckalter)  
#

# <a name="histogramme"></a>1. Histogramme
# ---
# Im folgenden Abschnitt werden wir damit beginnen, die in den Daten enthaltenen Informationen zu visualisieren. Eine sehr einfache Visualisierung ist das Histogramm, eine graphische Darstellung der Häufigkeit eines Merkmals. Zur Illustration erstellen wir eine kleine Visualisierung der Verteilung von Werten in einer Liste:

# +
# eine Liste mit 30 Ganzzahlen, die z.B. das Alter von Personen in einer
# Gruppe darstellen können
alter = [37, 20, 84,  2, 11, 89, 52, 65, 90, 21, 30, 12, 17, 53, 62, 45, 37,
        3,  6, 64, 85,  6, 95, 73, 68, 55, 86, 83, 91,  1]

# für Visualisierungen benutzen wir die Blbiothek "matplotlib.pyplot"
import matplotlib.pyplot as plt

# Histogramme lassen sich einfach mit der Funktion plt.hist() erstellen
plt.hist(alter);
# -

# Visualisierungen sollten immer mit Achsenbeschriftungen versehen werden. Dafür verwenden wir die Funktionen ```plt.xlabel()``` und ```plt.ylabel```. Außerdem können wir der Abbildung mit ```plt.title()``` einen Titel geben. Mit verschiedenen optionalen Argumenten lässt sich das Verhalten von ```plt.hist()``` anpassen:
# * ```bins``` ändert die Anzahl der Bins
# * ```range``` ändert den dargestellten Bereich auf der x-Achse
# * ```rwidth``` ändert die dargestellte bin-Weite 
# * ```color``` ändert die Farbe
# * ```alpha``` ändert den Alpha-Wert (Durchsichtigkeit) der dargestellten Balken.
#
# Plot-Funktionen haben oftmals sehr viele optionale Argumente (auch "keyword arguments" genannt). Um herauszufinden, wass die jeweiligen Funktionen alles können, lohnt sich ein Blick in die [Dokumentation](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.hist.html). Außerdem sollte jede Darstellung *immer* mit Achsenbeschriftungen und eventuell einem Titel versehen werden:

plt.hist(alter, color='darkred', rwidth=0.8, range=[0,100], bins=10, label='alter')
plt.xlabel('Alter')
plt.ylabel('Anzahl')
plt.title('Verteilung des Alters');

# [Anfang](#top)

# <a name="geschirrgroesse"></a>2. Größe des Geschirrs
# ---
# Als Vorbereitung lesen wir den Datensatz der Fundstücke, den wir in der letzten Übung lokal gespeichert haben wieder ein:

# +
# importiere pandas
import pandas as pd

# Hinweis: falls du die Datei im Unterordner "daten" abgelegt hast,
# musst du als Pfad "daten/ICRATES_CATALOGUE.csv" angeben
catalogue = pd.read_csv("../daten/ICRATES/ICRATES_CATALOGUE.csv", encoding='ISO-8859-1')
# -

# **A.** Erstelle ein Histogramm der in der Spalte ```Min_Rim_Diameter_mm``` enthaltenen Durchmesser. <font color='green'>**HINWEIS:** Damit das funktioniert, musst du mit der Funktion ```dropna()``` alle ```NaN```-Werte entfernen.</font>  

plt.hist(catalogue.Min_Rim_Diameter_mm.dropna() )
plt.show()

# **B.** Mache dich mit den unterschiedlichen Argumenten der Funktion ```plt.hist()``` vertraut, indem du verschiedene Werte und Kombinationen für die Daten ausprobierst. Finde Werte, die die Daten deiner Meinung nach gut und verständlich darstellen. Worauf kommt es dabei an? Versieh die Darstellung mit passenden Beschriftungen.  

plt.figure(figsize=(15,5))
plt.hist(catalogue.Min_Rim_Diameter_mm.dropna(), color='darkred', rwidth=0.8, bins=25, label='ICRATES Catalogue')
plt.xlabel('Min_Rim_Diameter [mm]')
plt.ylabel('Counts')
plt.legend()
plt.title('Verteilung der Randdurchmesser');

plt.hist(catalogue.Min_Rim_Diameter_mm.dropna(), bins=100)
plt.show()

plt.hist(catalogue.Min_Rim_Diameter_mm.dropna(), bins=100, histtype='step')
plt.show()

# **C.** Insbesondere das Argument ```bins``` verändert die Darstellung des Histogrammes sehr stark. Was kann schiefgehen, wenn ```bins``` zu niedrig oder zu hoch gewählt wird? Was ist eine gute Wahl für einen (diesen) Datensatz?  

# +
plt.hist(catalogue.Min_Rim_Diameter_mm.dropna(), 5)
plt.show()

plt.hist(catalogue.Min_Rim_Diameter_mm.dropna(), 25)
plt.show()

plt.hist(catalogue.Min_Rim_Diameter_mm.dropna(), 50)
plt.show()

plt.hist(catalogue.Min_Rim_Diameter_mm.dropna(), 100)
plt.show()

plt.hist(catalogue.Min_Rim_Diameter_mm.dropna(), 250)
plt.show()

plt.hist(catalogue.Min_Rim_Diameter_mm.dropna(), 2500)
plt.show()
# -

# Wählen wir die bin - Anzahl zu klein, verlieren wir Informationen, weil wir zu viele unterschiedliche Informationen zusammen fassen. Bei zu großen Werten im Gegensatz zeigen wir mehr oder weniger die einzelnen Werte für die Durchmesser direkt an. Beides erlaubt uns nicht, einen Trend in den Daten zu finden.
# Ein guter Wert, um einen Trend zu finden, ist hier zwischen 25 und 50, wir erkennen deutlich 2 Maxima in den Durchmessern.

# **D.** Visualisiere die Durchmesser an der Gefäßbasis (```Min_Base_Diameter_mm```) im selben Histogram. Was für Aussagen über die Gefäße kannst du aus dieser Visualisierung ableiten? <font color='green'>**HINWEIS:** Wenn du der Funktion ```plt.hist()``` nicht nur eine Liste von Werten sondern eine Liste von Listen übergibst, z.B. ```list_of_lists = [lower_date_list, upper_date_list]```, dann werden die Balken der beiden Listen im Histogramm nebeneinander dargestellt. Alternativ kannst du einfach in der selben Code-Zelle zwei mal hintereinander die Funktion ```plt.hist()``` (natürlich mit unterschiedlichen Daten) aufrufen, um die Daten übereinandergelegt darzustellen. Du kannst mit der Funktion ```plt.legend()``` eine Legende zur Abbildung hinzufügen. Damit das funktioniert, musst du den einzelen plot-Befehlen vorher ein entsprechendes ```label``` Argument mitgeben.</font> Was fällt dir auf? Worauf musst du achten, damit die in beiden Histogrammen dargestellten Werte vergleichbar sind? Warum ist diese Darstellung eventuell irreführend?  

plt.hist(catalogue.Min_Rim_Diameter_mm.dropna(), bins=40, range=[0,800], label='Rim')
plt.hist(catalogue.Min_Base_Diameter_mm.dropna(), bins=40, range=[0,800], label='Base')
plt.legend()
plt.show()

# * Damit die beiden Histogramme vergleichbar sind, bietet es sich an, jeweils die gleiche Anzahl an bins und den gleichen Bereich (```range=[min,manx]```) abzubilden. 
# * In dieser Darstellung sieht es so aus, als ob die Gefäße an der Basis im Schnitt deutlich kleiner sind als am oberen Rand. 
# * In dem Datensatz gibt es wesentlich weniger Einträge zu Basis-Durchbessern als zu Durchmessern vom oberen Rand. Das darsgestellte Ergebnis könnte als zum einen daran liegen, dass die Gefäße an der Basis tatsächlich kleiner sind. Eine alternative Erklärung wäre, dass eher Stücke von kleinen Gefäßen, aus denen sich der Basis-Durchmesser errechnen lässt, erhalten sind.

# **E. (Optional)** Was hat es mit den beiden keyword arguments ```density``` und ```cumulative``` auf sich? Wie verändert sich das Histogram und wie kann die Darstellung dann interpretiert werden?

# Die Dokumentation (```Shift + Tab```) zeigt uns folgendes:
#
# **density**:
# _the counts normalized to form a probability density, i.e., the area (or integral) under the histogram will sum to 1_
#     
# **cumulative**:
# _each bin gives the counts in that bin plus all bins for smaller values. The last bin gives the total number of datapoints_
#
#

plt.hist(catalogue.Min_Rim_Diameter_mm.dropna(), density=True)
plt.show()

plt.hist(catalogue.Min_Rim_Diameter_mm.dropna(), density=True, bins=100, cumulative=True)
plt.show()

plt.hist(catalogue.Min_Rim_Diameter_mm.dropna(), bins=40, range=[0,800], alpha=.5, density=True, label='Rim')
plt.hist(catalogue.Min_Base_Diameter_mm.dropna(), bins=40, range=[0,800], alpha=.5, density=True, label='Base')
plt.legend()
plt.show()

# [Anfang](#top)

# <a name="fundstueckalter"></a>3. Alter der Fundstücke
# ---
# Gefäße werden anhand ihrer Form und ihres Aussehens einer bestimmten Typologie zugeordnet. In der Tabelle ```ICRATES_STANDARD_FORM.csv``` sind Informationen über die Haupt-Typologien der Gefäße enthalten: Jede Typologie hat eine ID sowie einen Zeitraum, indem die Typologie in der Mittelmeerregion verbreitet war. Der Zeitraum ist durch eine untere und eine obere Jahresgrenze in der Tabelle dargestellt. Zusammen mit der Tabelle und der im Katalog für jedes Fundstück hinterlegten ```Standard_Form_ID``` lassen sich Rückschlüsse auf das Alter der Fundstücke ziehen. Auf der [Website des Datensatzes](https://archaeologydataservice.ac.uk/archives/view/icrates_lt_2018/overview.cfm) ist dokumentiert, wie der Datenbestand aufgebaut ist. Außerdem gibt es eine schöne Visualisierung, die die Zusammenhänge zwischen den verschiedenen Tabellen sehr deutlich macht:
#
# ![Bild](https://archaeologydataservice.ac.uk/archives/view/icrates_lt_2018/images/ICRATES_ER.png)

# Wir laden die Tabelle mit den Standard Formen:
standard_form = pd.read_csv("../daten/ICRATES/ICRATES_STANDARD_FORM.csv")#, index_col='Standard_Form_ID')

# **A. (optional)** Kannst du Beispiele (vielleicht sogar Bilder?) für die Typologien finden und erklären, woran man sie erkennt? <font color='green'>**HINWEIS:** Diese Aufgabe ist insbesondere für Archäologie-Student*innen gedacht, die in ihrem Studium eventuell schon mit diesem domain-knowledge (Fachwissen) in Berührung gekommen sind.</font>  

# # <font color='red'>  @Manuel </font>



# **B.** Finde für jedes Fundstück im Katalog den Zeitraum, aus dem das Fundstück stammt:
# * Iteriere über alle Fundstücke des Kataloges
# * Finde heraus, welche ```Standard_Form_ID``` dem Fundstück zugewiesen ist. <font color='green'>**HINWEIS:** Nicht allen Fundstücken ist eine "Standard Form" zugewiesen. Deswegen solltest du überprüfen, ob der Eintrag vorhanden ist. Das geht mit der Funktion ```pd.isna(wert)```. </font> 
# * Benutze die ```Standard_Form_ID```, um in der Tabelle der Standard Forms die untere und obere Grenze des Zeitraumes herauszufinden, aus dem das Fundstück stammt.
# * Speichere diese Information in zwei Listen ab.
# * Füge die Listen dem Katalog hinzu. <font color='green'>**HINWEIS:** Damit das funktioniert, müssen die Listen genauso viele Einträge enthalten wie der Katalog Zeilen hat. Entsprechend musst du für Fundstücke, denen keine Standard Form zugewiesen ist ```nan``` in der Liste speichern.</font>

# den nan-Wert besorgen wir uns aus der Bibliothek numpy:
import numpy as np
np.nan

# +
# zwei leere Listen:
upper = []
lower = []

# wir iterieren ueber alle Standard Form IDs
for ID in catalogue.Standard_Form_ID:
    if pd.isna(ID): # wenn keine ID verfuegbar ist, packen wir nan in die liste
        upper.append(np.nan)
        lower.append(np.nan)
    else:           # sonst den passenden Wert aus der Formen Tabelle
        mask = standard_form['Standard_Form_ID'] == ID
        u = standard_form[mask]['Standard_Typo_chronological_Upper_Date'].item()
        l = standard_form[mask]['Standard_Typo_chronological_Lower_Date'].item()
        upper.append(u)
        lower.append(l)
# -

# Die beiden Listen koennen wir nun als weitere Spalten in den Katalog anhaengen
# google/duckduckgo: pandas cheat sheet ;) 
catalogue['upper'] = upper
catalogue['lower'] = lower
catalogue.to_csv("../daten/ICRATES/ICRATES_CATALOGUE.csv")

# **C.** Was ist das mittlere geschätzte Alter der Fundstücke?

# ein kurzer Blick in den Katalog mit den neuen Spalten:
catalogue.loc[:10,['upper', 'lower']].dropna()

# aktuelles Datum
import datetime
now = datetime.datetime.now()

# alter ist dieses Jahr - Jahr der Herstellung
age = now.year - ( catalogue.loc[:,'upper'] + catalogue.loc[:,'lower'] )/2

# ein paar Alter:
age.dropna()[:10]

# Mittleres Alter:
age.mean()

# **D. (optional)** Was ist die mittlere Länge der in der Standard Form Tabelle hinterlegten Zeiträume?

# Zeitraumlaenge:
diff = catalogue.loc[:,'upper'] - catalogue.loc[:,'lower']
diff.dropna()[:10]

# Mittlere Laenge:
round(diff.mean(),1)

# **E.** Visualisiere die Verteilung der oberen Zeitraumsgrenzen in einem Histogram. Visualisiere die Verteilung der unteren Zeitraumsgrenzen in einem Histogram. Vergiss nicht auf Achsenbeschriftungen etc., achte darauf, dass ```bins``` sinnvoll gewählt ist.

max(diff.dropna())

plt.figure(figsize=(15,5))
plt.hist(diff.dropna(), color='darkred', rwidth=0.8, bins=40, label='Hinterlegte Zeitraeume')
plt.xlabel('Zeitraum [Jahre]')
plt.ylabel('Counts')
# plt.yscale('log')
plt.legend()
# plt.title('Hinterlegte Zeitraeume');
plt.show()

plt.figure(figsize=(15,5))
plt.hist(age.dropna(), color='darkred', rwidth=0.8, bins=50, label='Alter der Scherben')
plt.xlabel('Alter [Jahre]')
plt.ylabel('Counts')
# plt.yscale('log')
plt.legend()
# plt.title('Alter der Scherben');
plt.show()

# **F.** Interpretiere die Visualisierung: Kannst du dein Wissen über historische Ereignisse (oder über die Art und Weise, wie Grabungen durchgeführt werden) dafür benutzen, um das Ergebnis zu erklären?

# # <font color='red'>  @Manuel </font>



# [Anfang](#top)
