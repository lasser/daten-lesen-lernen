# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import matplotlib.pyplot as plt
import pandas as pd
from os.path import join

# Hier erhalten Sie eine Menge Datenreihen, darunter allgemeine volkswirtschaftliche, gewerbliche und landwirtschaftliche Daten. Auch Zahlenreihen zu institutionellen Prozessen lassen sich hier finden. Interessant ist sicherlich die die lange Datenreihe zum GDP per capita (1500 bis 2010). (Forschungsfrage: Wie entwickelte sich der individuelle Wohlstand in den letzten 500 Jahren?) Daran ließe sich grundsätzlich z.B. diskutieren, inwiefern GDP überhaupt ein geeigneter Indikator für eine solche Fragestellung ist. Aus historischer Perspektive kommt die Problematik hinzu, wie aussagekräftig eigentlich derlei Langzeitdaten sind, wenn man weiß, dass es das Konzept GDP erst seit den 1930er Jahren gibt und statistische Ämter, etwa in Deutschland oder Frankreich, seit etwa 1800.
#
#
#
# Denkbar ist die Übernahme anderer Datenreihen und eine ähnliche Indikatordiskussion, z.B. Ist der Anstieg der Fleischproduktion ein Ausweis für steigenden Wohlstand? Oder sinkende Indikator für Armut? Auch ließ sich die Aussagekraft verschiedener Gender-Equality-Indikatoren in ihren Grenzen und Möglichkeiten diskutieren, also inwiefern sie tatsächlich Auskunft über Ungleichheiten geben. Welcher Indikator eignet sich hier besser, welcher weniger gut - oder, präziser, welcher für welche Aussage.
#
#

# General source: https://clio-infra.eu/

# Streik-Daten: WSI Hans Böckler Stiftung (Daten aus Arbeitnehmersicht) - Lohndatenbank

# # GDP

GDP = pd.read_csv('/home/jana/DaLeLe/uebungen/daten/wiwi/GDPperCapita_Broad.csv')
GDP.drop(columns=['ccode'], inplace=True)
GDP.rename(columns={'country name':'country'}, inplace=True)
GDP.head(3)

fname_tage = 'NumberofDaysLostinLabourDisputes_Broad.xlsx'
tage = pd.read_excel(join("daten/", fname_tage))
tage.drop(columns=['ccode'], inplace=True)
tage.rename(columns={'country name':'country'}, inplace=True)
tage.head(3)

# +
pop = pd.read_excel('daten/Data_Extract_From_World_Development_Indicators.xlsx')
#pop.drop(columns = ['Series Name', 'Series Code', 'Country Code'], inplace=True)
#pop.rename(columns={c:'{}_pop'.format(i) for c,i in zip(pop.columns[1:],range(1960, 2019))}, inplace=True)
#pop = pd.melt(pop, id_vars=['Country Name'], value_vars=['{}'.format(i) for i in range(1960, 2019)],\
#       var_name = 'year', value_name = 'population')

#pop.to_csv('daten/world_population_long.csv')
pop.rename(columns={'country':'country name'}, inplace=True)
#pop.drop(columns=['{}_pop'.format(i) for i in range(2016, 2019)], inplace=True)
pop.to_excel('daten/Data_Extract_From_World_Development_Indicators.xlsx')
pop.head()
# -

GDP.drop(columns=['{}'.format(i) for i in range(1500, 1960)], inplace=True)
GDP.rename(columns={'{}'.format(i):'{}_GDP'.format(i) for i in range(1960, 2016)}, inplace=True)
GDP.head()

tage.drop(columns=['{}'.format(i) for i in range(1500, 1960)], inplace=True)
tage.rename(columns={'{}'.format(i):'{}_days'.format(i) for i in range(1960, 2016)}, inplace=True)
tage.head()

merged = tage.merge(pop, how='inner', on=['country'])
merged = merged.merge(GDP, how='inner', on=['country'])
merged.head()

# +
# normalized strike days
for i in range(1960, 2016):
    merged['{}_norm'.format(i)] = merged['{}_days'.format(i)] / merged['{}_pop'.format(i)]
    
merged.head()
# -

country = 'United Kingdom'
merged_country = merged[merged['country'] == country]
merged_country

import matplotlib.pyplot as plt
years = list(range(1960, 2015))
plt.plot(years, merged_country[['{}_norm'.format(i) for i in years]].values[0], label='mit Bevölkerungsverlauf normalisiert')
plt.plot(years, merged_country[['{}_days'.format(i) for i in years]].values[0] / 1.8e8, label='mit Wert von 1960 normalisiert')
plt.plot(years, merged_country[['{}_days'.format(i) for i in years]].values[0] / 3.2e8, label='mit Wert von 2015 normalisiert')
plt.xlabel('year')
plt.ylabel('strike days / inhabitant')
plt.legend()

years = list(range(1960, 2015))
plt.plot(years, merged_country[['{}_days'.format(i) for i in years]].values[0])
plt.xlabel('year')
plt.ylabel('strike days')

years = list(range(1960, 2015))
plt.plot(years, merged_country[['{}_pop'.format(i) for i in years]].values[0])
plt.xlabel('year')
plt.ylabel('population')

# +
window = 20
start = 1980
averages = merged[['{}_days'.format(i) for i in range(start, start + window)]].mean(axis=1)

temp_df = pd.DataFrame({'country':merged['country'], 'strikes':averages, 'population':merged['{}_pop'.format(start + window)].copy()})
temp_df.dropna(inplace=True)

plt.scatter(temp_df['strikes'], temp_df['population'])
#for name, x, y in zip(temp_df['country'], temp_df['strikes'], temp_df['population']):
#    plt.text(x, y, name, fontsize=8)
plt.xlabel('strike days')
plt.ylabel('population')
plt.title('Strike-days averaged from {} to {}'.format(start, start + window))
plt.xlim(0, 1e7)
plt.ylim(0,0.4*1e9)

#print(rvalue)
#print(pvalue)
# -

slope, intercept, rvalue, pvalue, stderr = scipy.stats.linregress(temp_df['strikes'], temp_df['population'].astype(float))
print(pvalue)

# +
window = 10
start = 1960
averages = merged[['{}_norm'.format(i) for i in range(start, start + window)]].mean(axis=1)

temp_df = pd.DataFrame({'country':merged['country'], 'strikes':averages, 'GDP':merged['{}_GDP'.format(start + window)].copy()})
temp_df.dropna(inplace=True)

plt.scatter(temp_df['strikes'], temp_df['GDP'])
for name, x, y in zip(temp_df['country'], temp_df['strikes'], temp_df['GDP']):
    plt.text(x, y, name, fontsize=8)
plt.xlabel('strike days / capita')
plt.ylabel('GDP / capita')
plt.title('Strike-days averaged from {} to {}'.format(start, start + window))

# +
import numpy as np
import scipy.stats
slope, intercept, rvalue, pvalue, stderr = scipy.stats.linregress(temp_df['strikes'], temp_df['GDP'])

plt.scatter(averages, merged['1990_GDP'])
x = np.arange(0,0.3, 0.01)
plt.plot(x, intercept + x * slope, '--', color='red')
plt.xlabel('strike days / capita')
plt.ylabel('GDP / capita')
plt.xlim(0,0.3)
plt.ylim(0,25000)
print(pvalue)
print(rvalue)
# -

country = 'United States'
GDP_country = GDP[GDP['name'] == country]

GDP_US = GDP[GDP['name'] == 'United States']
GDP_japan = GDP[GDP['name'] == 'Japan']

plt.scatter(GDP['year'], GDP['value'])
plt.scatter(GDP_US['year'], GDP_US['value'])
plt.scatter(GDP_japan['year'], GDP_japan['value'])
plt.xlim(1800,2020)

GDP['value'].max()

names = list(GDP.name.unique())
names.sort()

mask_GDP_US = GDP['name'] == 'United States' 
GDP_US = GDP[mask_GDP_US]

plt.plot(GDP_US['year'], GDP_US['value'], 'o-')

# # Labor Disputes

# demographie-Daten
pop1 = pd.read_excel('daten/TotalPopSex-20190519040514.xlsx', sheet_name='Data', header=1)
pop2 = pd.read_excel('daten/TotalPopSex-20190519040623.xlsx', sheet_name='Data', header=1)
pop = pd.concat([pop1, pop2[pop2.columns[4:]]], axis=1)
new_column_names = {year:'{}_population'.format(year) for year in pop.columns[4:]}
#pop.rename(columns=new_column_names,inplace=True)
pop.rename(columns={'ISO 3166-1 numeric code':'ccode'},inplace=True)
pop.head()

pop = pd.melt(pop, id_vars=['Location','ccode'], value_vars=pop.columns[4:], var_name='year', value_name='population')
pop.to_csv('daten/world_population_1950_to_2019.csv')

pop.head()

data_path = '/home/jana/DaLeLe/uebungen/daten/wiwi/'
fname_days = 'NumberofDaysLostinLabourDisputes_Broad.xlsx'
fname_disputes = 'NumberofLabourDisputes_Broad.xlsx'
fname_workers = 'NumberofWorkersInvolvedinLabourDisputes_Broad.xlsx'
days = pd.read_excel(join(data_path, fname_days), sheet_name='Data Long Format')
disputes = pd.read_excel(join(data_path, fname_disputes), sheet_name='Data Long Format')
workers = pd.read_excel(join(data_path, fname_workers), sheet_name='Data Long Format')

disputes.rename(columns={'value':'disputes','country.name':'Location'},inplace=True)
disputes.head()

# ### Aggregate data

workers.rename(columns={'value':'workers_in_disputes','country.name':'Location'},inplace=True)
days.rename(columns={'value':'days_in_disputes','country.name':'Location'},inplace=True)

agg = disputes.set_index('ccode').join(days.set_index('ccode')['days_in_disputes'])

agg = agg.join(workers.set_index('ccode')['workers_in_disputes'])

agg.head()

selection_workers = (workers['year'] == 2000) & (workers['Location'] == 'Austria')
selection_disputes = (disputes['year'] == 2000) & (disputes['Location'] == 'Austria')

disputes[selection_disputes]

agg = pop.set_index('ccode').join(disputes.set_index('ccode'),lsuffix='_population',rsuffix='_disputes')
#print(agg.head())
len(agg.dropna())

# +
#disputes.columns = ['ccode', 'name', 'year', 'disputes_value']
#workers.columns = ['ccode', 'name', 'year', 'workers_value']
#days.columns = ['ccode', 'name', 'year', 'days_value']
# -

country = 'Brazil'
days_country = days[days['country.name'] == country]
disputes_country = disputes[disputes['country.name'] == country]
workers_country = workers[workers['country.name'] == country]

disputes_GB = disputes[disputes['country.name'] == 'United Kingdom']
disputes_france = disputes[disputes['country.name'] == 'France']
disputes_italy = disputes[disputes['country.name'] == 'Italy']
disputes_spain = disputes[disputes['country.name'] == 'Spain']

plt.plot(disputes_GB['year'], disputes_GB['value'],'-o', label='United Kingdom')
plt.plot(disputes_italy['year'], disputes_italy['value'],'-o', label='Italy')
plt.plot(disputes_france['year'], disputes_france['value'],'-o', label='France')
plt.plot(disputes_spain['year'], disputes_spain['value'],'-o', label='Spain')
plt.xlim(1960, 2010)
plt.legend()

means = []
stds = []
number = []
medians = []
for year in range(1970,2006):
    tmp_filter = disputes['year'] == year
    year_disputes = disputes[tmp_filter]
    means.append(year_disputes['disputes'].mean())
    medians.append(year_disputes['disputes'].median())
    stds.append(year_disputes['disputes'].std())
    number.append(len(year_disputes))

plt.plot(range(1970,2006), means)
#plt.plot(range(1970,2006), medians)
plt.plot(range(1970,2006), stds)
#plt.plot(range(1970,2006), number)

means_workers = []
stds_workers = []
number_workers = []
medians_workers = []
for year in range(1970,2006):
    tmp_filter = workers['year'] == year
    year_workers = workers[tmp_filter]
    means_workers.append(year_workers['workers_in_disputes'].mean())
    medians_workers.append(year_workers['workers_in_disputes'].median())
    stds_workers.append(year_workers['workers_in_disputes'].std())
    number_workers.append(len(year_workers))

plt.plot(range(1970,2006), means_workers)
#plt.plot(range(1970,2006), medians_workers)
plt.plot(range(1970,2006), stds_workers)
#plt.plot(range(1970,2006), number)

means_days = []
stds_days = []
number_days = []
medians_days = []
for year in range(1970,2006):
    tmp_filter = days['year'] == year
    year_days = days[tmp_filter]
    means_days.append(year_days['days_in_disputes'].mean())
    medians_days.append(year_days['days_in_disputes'].median())
    stds_days.append(year_days['days_in_disputes'].std())
    number_days.append(len(year_days))

plt.plot(range(1970,2006), means_days)
#plt.plot(range(1970,2006), medians_workers)
plt.plot(range(1970,2006), stds_days)
#plt.plot(range(1970,2006), number)

import numpy as np
plt.plot(range(1970,2006), np.asarray(means_workers) / np.asarray(means))

N_total = len(disputes['country.name'].unique())

# +
labels = ['Daten vorhanden', 'Daten nicht vorhanden']
filter_2000 = disputes['year'] == 2000
disputes_2000 = disputes[filter_2000]
N_data = len(disputes_2000)
N_nodata = N_total - N_data

sizes = [N_data / N_total * 100, N_nodata / N_total * 100]
# -

plt.pie(sizes, labels=labels, autopct='%1.1f%%')
plt.axis('equal')
plt.title('Datenlage im Jahr 2000');

fi = (disputes_france['year'] >= 1980) & (disputes_france['year'] <= 2010)
disputes_france['value'][fi].mean()

days.head()

country = 'United States'
days_country = days[days['Location'] == country]
workers_country = workers[workers['Location'] == country]
disputes_country = disputes[disputes['Location'] == country]

plt.plot(days_country['year'], days_country['days_in_disputes'],\
         '-o', label='days')
plt.plot(workers_country['year'], workers_country['workers_in_disputes'],\
         '-o', label='workers')
plt.plot(disputes_country['year'], disputes_country['disputes'],\
         '-o',label='disputes')
plt.yscale('log')
plt.legend()

workers_days = workers_country.merge(days_country,how='left', indicator=True)

plt.plot(workers_days['year'], \
         workers_days['days_value'] / workers_days['workers_value'],\
         '-o', label='days per worker')
plt.legend()

plt.scatter(workers_days['days_value'], workers_days['workers_value'])

# Observations 
# * informtion gets lost beause values are not comparable
# * for the US, there is a huge drop in number of disputes in the ninties but no significant drop in the involved workers. What happened there?
# * huge values of days / worker in the 1930 in the US: is that realistic?
# * pretty strong correlation between days and involved workers for the US
# * for Germany, there is a huge gap between the 30s and the 90s: what happened there? Can you fill in the gap by using the historical data?
# * for Switzerland, there are some hughe spikes in days / worker in the 30s, 50s and 60s: what happened there?

workers_germany['year']

# ### Visualize disputes over countries

import geopandas as gpd
# quelle: http://thematicmapping.org/downloads/world_borders.php
path = "daten/TM_WORLD_BORDERS_SIMPL-0.3/TM_WORLD_BORDERS_SIMPL-0.3.shp"
world_map = gpd.read_file(path)
world_map.head()

disputes.head()

# +
map_codes = set(world_map.UN.unique())
disp_codes = set(disputes.ccode.unique())

len(map_codes.intersection(disp_codes))
# -

joined = world_map.set_index('UN').join(disputes.set_index('ccode'))
joined['test'] = range(len(joined))
joined.head()

variable = "2010"
vmin, vmax = 120, 220
fig, ax = plt.subplots(1, figsize=(10, 6))
#joined.plot(column='test', cmap='Blues', linewidth=0.8, ax=ax, edgecolor=0.8)

fig, ax = plt.subplots()
world_map.plot(ax = ax)
#ax.scatter(10, 55, color='red')

disputes_germany['year']

disputes_workers = disputes_germany.merge(workers_germany, how='left', indicator=True)
disputes_days = disputes_germany.merge(days_germany, how='left', indicator=True)
workers_days = workers_germany.merge(days_germany,how='left', indicator=True)

workers_days.head()

plt.scatter(disputes_workers['disputes_value'], disputes_workers['workers_value'])

plt.scatter(workers_days['days_value'], workers_days['workers_value'])

# ## UN data

# zum Laden von Daten aus dem Internet
import requests 
import io

# +
url_managers = 'https://opendata.arcgis.com/datasets/ee7c0813c1264175a0efad4cacc0ce23_0.csv'

antwort_managers = requests.get(url_managers).content
inhalt_managers = io.StringIO(antwort_managers.decode('ISO-8859-1'))

# lade die Daten in ein DataFrame und zeige die ersten paar Zeilen an
data_managers_raw = pd.read_csv(inhalt_managers)
data_managers_raw.head()

# +
url_GDP = 'https://opendata.arcgis.com/datasets/15f94efdb87642a3b2763416f71d1329_0.csv'
antwort_GDP = requests.get(url_GDP).content
inhalt_GDP = io.StringIO(antwort_GDP.decode('ISO-8859-1'))

# lade die Daten in ein DataFrame und zeige die ersten paar Zeilen an
data_GDP = pd.read_csv(inhalt_GDP)
data_GDP.head()
# -

data_GDP.index[data_GDP['geoAreaName'] == 'Syrian Arab Republic (Syria)'].tolist()

for name in data_GDP['geoAreaName']:
    if name.startswith('Ch'):
        print(name)

plt.plot(range(2000,2018),data_GDP.loc[93][11:29])

# +
year_columns = data_GDP.columns[11:29]

mean_GDP = pd.DataFrame()
means = []
stds = []
for c, y in zip(year_columns, range(2000,2018)):
    means.append(data_GDP[c].mean())
    stds.append(data_GDP[c].std())
    
mean_GDP['year'] = range(2000,2018)
mean_GDP['GDP_mean'] = means
mean_GDP['GDP_std'] = stds
# -

index_2016_max = data_GDP.index[data_GDP['F2016'] ==\
                data_GDP['F2016'].max()].tolist()[0]
data_GDP['geoAreaName'][index_2016_max]

# +
index_syria = data_GDP.index[data_GDP['geoAreaName'] ==\
                'Syrian Arab Republic (Syria)'].tolist()[0]

index_china = data_GDP.index[data_GDP['geoAreaName'] ==\
                'China'].tolist()[0]

index_timorleste = data_GDP.index[data_GDP['geoAreaName'] ==\
                'Timor-Leste'].tolist()[0]

column_start_year = 11
column_end_year = 29

# +
plt.plot(mean_GDP['year'], mean_GDP['GDP_mean'], '-o',label='mean GDP')
plt.plot(mean_GDP['year'], mean_GDP['GDP_std'], '-o',label='std of GDP')
plt.plot(range(2000,2018),\
         data_GDP.loc[index_syria][column_start_year:column_end_year],\
         '-o', label='GDP of Syria')

#plt.plot(range(2000,2018),\
#         data_GDP.loc[index_timorleste][column_start_year:column_end_year],\
#         '-o',label='GDP of Timor-Leste')

plt.xticks(range(2000,2019,2))
plt.xlabel('year')
plt.ylabel('GDP')
plt.legend()
# -

# Beobachtungen:
# * Um das Jahr 2008 geht das mittlere GDP stark nach unten (Finanzkrise), die Varianz bleibt aber auf einem relativ niedrigen Niveau. Was bedeutet das?
# * In den Jahren 2012 und 2016 gibt es zwei starke Spikes in der GDP-Varianz, während das mittlere GDP recht konstant bleibt. Was bedeutet das?
# * Gibt es ein Land, dass sich dem allgemein negativen Trend um 2008 widersetzt?
# * Welche Länder tragen am stärksten zur erhöhten Varianz um 2012 / 2016 bei und in welche Richtung? Welche politischen Ereignisse sind eventuell damit verknüpft?
# * What the fuck happened with Timor-Leste?

data_managers_raw['Type_of_occupation_description'].unique()

# +
st = 'Type_of_occupation_description'

data_managers = data_managers_raw[\
    (data_managers_raw[st] == 'managers (isco-08)') |\
    (data_managers_raw[st] == 'professionals (isco-08)')]

data_managers = data_managers[data_managers['Sex'] == 'BOTHSEX']

# +
mean_salary_managers = pd.DataFrame()
means = []
stds = []

data = data_managers[data_managers[st] == 'managers (isco-08)']

year_columns = data_managers.columns[15:32]

for c, y in zip(year_columns, range(2000,2017)):
    means.append(data[c].mean())
    stds.append(data[c].std())
    
mean_salary_managers['year'] = range(2000,2017)
mean_salary_managers['salary_mean'] = means
mean_salary_managers['salary_std'] = stds

# +
mean_salary_professional = pd.DataFrame()
means = []
stds = []

data = data_managers[data_managers[st] == 'professionals (isco-08)']

year_columns = data_managers.columns[15:32]

for c, y in zip(year_columns, range(2000,2017)):
    means.append(data[c].mean())
    stds.append(data[c].std())
    
mean_salary_professional['year'] = range(2000,2017)
mean_salary_professional['salary_mean'] = means
mean_salary_professional['salary_std'] = stds

# +
plt.plot(mean_salary_managers['year'], mean_salary_managers['salary_mean'],\
         '-o',label='mean salary manager')
plt.plot(mean_salary_professional['year'], mean_salary_professional['salary_mean'],\
         '-o',label='mean salary professional')

plt.xticks(range(2000,2019,2))
plt.xlabel('year')
plt.ylabel('salary')

plt.legend()

# +
plt.plot(mean_salary_managers['year'], mean_salary_managers['salary_mean'] /\
         mean_salary_professional['salary_mean'],\
         '-o',label='normed salary')

plt.xticks(range(2000,2019,2))
plt.xlabel('year')
plt.ylabel('salary')

plt.legend()
# -

# Ergibt es Sinn, den Durchschnitt zu berechnen, wenn das Gehalt in lokaler Währung angegeben ist? $\rightarrow$ besser das Gehalt in Einheiten Gehalt eines ungelernten Arbeiters des selben Landes anzugeben!

# ## Welche Berufsgruppe hat das geringste Einkommen?

occ_types = data_managers_raw['Type_of_occupation_description'].unique()

# +
types = []
occ_types_averages = []
occ_types_std = []
years = []

st = 'Type_of_occupation_description'

year_columns = ['F{}'.format(y) for y in range(2000,2017)]

occ_data = data_managers_raw[ data_managers_raw['Sex'] == 'BOTHSEX']

for occ in occ_types:
    for c, y in zip(year_columns, range(2000, 2017)):
        avg = occ_data[occ_data[st] == occ][c].mean()
        std = occ_data[occ_data[st] == occ][c].std()
    
        occ_types_averages.append(avg)
        occ_types_std.append(std)
        years.append(y)
        types.append(occ)
# -

salary_averages = pd.DataFrame({'occupation':types,\
                               'year':years,\
                               'salary_mean':occ_types_averages,\
                               'salary_std': occ_types_std})
salary_averages.head()

# +
plt.bar(range(len(occ_types)), \
        salary_averages[salary_averages['year']==2016]['salary_mean'])

labels = [t.split('(')[0] for t in occ_types]

plt.xticks(range(len(occ_types)), labels, rotation='vertical');
# elementary occupations seem to be the lowest payed jobs
# -

salary_averages.head()

# +
lower = 'professionals (isco-08)'
managers = 'managers (isco-08)'

year_columns = ['F{}'.format(y) for y in range(2000,2017)]

for c in year_columns:
    manager_salary = data_managers_raw[data_managers_raw[st] == managers][c]
    lower_salary = data_managers_raw[data_managers_raw[st] == lower][c]
    norm = []
    print('lower: {:1.2f}'.format(lower_salary.sum()))
    print('manager: {:1.2f}'.format(manager_salary.sum()))

    for m, l in zip(manager_salary, lower_salary):
        norm.append(m / l)
    
    print(len(norm))
    print(len(data_managers))
    data_managers[c + '_normed'] = norm
    print('normed: {:1.2f}'.format(norm.sum()))

# -

data_managers_normed = data_managers_raw[data_managers_raw[st] == 'managers (isco-08)']
data_managers_normed.columns

# +
normed_mean_salary = pd.DataFrame()

means = []
stds = []

year_columns = ['F{}_normed'.format(y) for y in range(2000,2017)]

for c, y in zip(year_columns, range(2000,2017)):
    means.append(data_managers_normed[c].mean())
    stds.append(data_managers_normed[c].std())
    
normed_mean_salary['year'] = range(2000,2017)
normed_mean_salary['salary_mean'] = means
normed_mean_salary['salary_std'] = stds

# +
#plt.plot(mean_salary['year'], mean_salary['salary_mean'],\
#         '-o',label='mean salary')
plt.plot(normed_mean_salary['year'], normed_mean_salary['salary_mean'],\
         '-o',label='normed mean salary')

plt.xticks(range(2000,2019,2))
plt.xlabel('year')
plt.ylabel('salary')
plt.legend()
# -

index_managers_ger = data_managers.index[data_managers['geoAreaName'] ==\
                'Germany'].tolist()[0]

# +
tidy_GDP = pd.DataFrame()
countries = []
years = []
GDPs = []

for i in range(len(data_GDP)):
    for c,y in zip(year_columns, range(2000,2018)):
        countries.append(data_GDP.loc[i]['geoAreaName'])
        years.append(y)
        GDPs.append(data_GDP.loc[i][c])
        

# +
tidy_GDP['country'] = countries
tidy_GDP['year'] = years
tidy_GDP['GDP'] = GDPs

tidy_GDP.tail()

# +
for index, year in enumerate(range(2000,2018)):
    plt.scatter(tidy_GDP['year'], tidy_GDP['GDP'])

plt.ylim(-50,50)
# -

# ## Stock data

# source: https://www.measuringworth.com/datasets/DJA/
DJIA = pd.read_csv('/home/jana/DaLeLe/uebungen/daten/wiwi/DJA.csv',skiprows=range(4))
DJIA.head()

plt.plot(DJIA['DJIA'])
start = 1896
end = 2019
labels = [str(i) for i in range(1900,2019,10)]
plt.xticks(range(4*365,len(DJIA),10*365),labels,rotation='vertical');

differences = [DJIA.loc[i + 1]['DJIA'] - DJIA.loc[i]['DJIA']\
              for i in range(len(DJIA)-1)]

differences.append(0)
DJIA['differences'] = differences

year = [int(DJIA.loc[i]['Date'].split('/')[2]) for i in range(len(DJIA))]
DJIA['year'] = year

day = [int(DJIA.loc[i]['Date'].split('/')[1]) for i in range(len(DJIA))]
DJIA['day'] = day

month = [int(DJIA.loc[i]['Date'].split('/')[0]) for i in range(len(DJIA))]
DJIA['month'] = month

DJIA.head()

years = range(1896,2020)

# +
year_avg = pd.DataFrame()
avg = []
for year in years:
    avg.append(DJIA[DJIA['year'] == year]['DJIA'].mean())
    
year_avg['year'] = years
year_avg['average'] = avg

year_avg.head()

# +
month_avg = pd.DataFrame()
avg = []
months = range(1, 13)
month_list = []
year_list = []

for year in years:
    for month in months:
        avg.append(DJIA[(DJIA['year'] == year) & \
                  (DJIA['month'] == month)]['DJIA'].mean())
        month_list.append(month)
        year_list.append(year)
    
month_avg['year'] = year_list
month_avg['month'] = month_list
month_avg['average'] = avg


# +
differences_month = [month_avg.loc[i + 1]['average'] - \
                     month_avg.loc[i]['average']\
              for i in range(len(month_avg)-1)]

differences_month.append(0)
month_avg['differences'] = differences_month

# +
month_normed = []

for year in years:
    for month in months:
        month_val = list(month_avg[(month_avg['year'] == year) & \
                        (month_avg['month'] == month)]['average'])[0]
        year_val = list(year_avg[year_avg['year'] == year]['average'])[0]
        
        norm = month_val / year_val
        month_normed.append(norm)
# -

month_avg['average_normed'] = month_normed
month_avg.tail()

# +
month_overview = pd.DataFrame()

avg = []
std = []

for month in range(1,13):
    avg.append(month_avg[month_avg['month'] == month]['average_normed'].mean())
    std.append(month_avg[month_avg['month'] == month]['average_normed'].std())
    
month_overview['month'] = range(1, 13)
month_overview['average_normed'] = avg
month_overview['std_normed'] = std

month_overview.head(12)
# -

plt.bar(range(1,13), month_overview['average_normed'] - 1.)
plt.xlabel('month')
plt.ylabel('monthly average compared to annual average')
plt.xticks(range(1,13));

plt.bar(range(1,13), month_overview['std_normed'])
plt.xlabel('month')
plt.ylabel('std')
plt.xticks(range(1,13));

plt.plot(month_avg['average_normed'])

plt.plot(month_avg['average'])

plt.plot(month_avg['differences'])

plt.plot(DJIA['differences'])


