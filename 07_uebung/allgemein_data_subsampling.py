# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import pandas as pd
from random import sample
from os.path import join
import pandas as pd

# +
path = "/home/jana/DaLeLe/uebungen/daten-lesen-lernen-repo/daten"
data_name = 'tweets_troll_all'

tweets_trump = pd.read_csv(join(path, 'tweets_trump.csv'))
tweets_dataset = pd.read_csv(join(path, data_name + ".csv"))
# -

# number of Trump tweets. We want our subset to include the
# same number of data so we do not need to care about normalization
N = len(tweets_trump)
print(N)

# draw a random sample from the index list
random_sample_mask = sample(list(tweets_troll.index), N)

# sort the sample (not scrictly necessary) and
# look at the first entries to sanity check
random_sample_mask.sort()
random_sample_mask[0:5]

# mask the dataframe with the random sample
sampled_df = tweets_troll[tweets_troll.index.isin(random_sample_mask)]
# sanity check again
sampled_df.head()

# save subset
sampled_df.to_csv(join(path, data_name + '_subset.csv'))
