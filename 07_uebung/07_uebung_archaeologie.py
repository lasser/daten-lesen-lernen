# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _Lizenz: Das folgende Lehrmaterial kann unter einer [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) Lizenz frei verwendet, verbreitet und modifiziert werden._   
#  _Authoren: Jana Lasser (jana.lasser@ds.mpg.de)_  
#  _Das Lehrmaterial wurde im Zuge des Projektes "Daten Lesen Lernen", gefördert vom Stifterverband und der Heinz Nixdorf Stiftung erstellt._

# # Übung 07 - Archäologie: Visualisierung über einer Landkarte
# ### Praktische Hinweise
# $\rightarrow$ Übungen sind dafür gedacht, dass ihr sie euch daheim anseht und versucht, ein paar Aufgaben selbst zu lösen. In den Tutorien könnt ihr euch weiter mit dem Lösungen der Aufgaben beschäftigen und dabei Hilfe von den Tutor*innen bekommen.  
#
# $\rightarrow$ Wenn ein Fehler auftritt:
# 1. Fehlermeldung _lesen_ und _verstehen_
# 2. Versuchen, selbst eine Lösung zu finden ($\rightarrow$ hier findet das Lernen statt!)
# 3. Das Problem googlen (Stichwort: Stackoverflow) oder den/die Nachbar*in fragen
# 4. Tutor*in fragen
#
# $\rightarrow$ Unter <font color='green'>**HINWEIS**</font> werden Hinweise gegeben, die für die Lösung der Aufgabe hilfreich sind und oftmals auch weiterführende Informationen zur Aufgabe liefern.
#
# $\rightarrow$ Mit **(Optional)** gekennzeichnete Übungsteile sind für besonders schnelle Menschen :-).

# <a name="top"></a>Inhalt
# ---
# In diesem Tutorium möchten wir uns damit beschäftigen, Daten denen eine Räumliche Information zugeordnet ist auf einer Landkarte zu visualisieren. Dafür beschäftigen wir uns zuerst mit sog. "Scatterplots", die es uns erlauben, Datenpunkte anhand ihrer x- und y-Koordinaten darzustellen. Dieses Wissen wenden wir dann an, um die Anzahl der Fundstücke im Mittelmehrraum graphisch darzustellen.
#
# Das Tutorial gliedert sich in
# * [Scatterplots und Orte](#spatterplots)
# * [Landkarten](#landkarten)

# <a name="scatterplots"></a>1. Scatterplots und Orte
# ---
# Die Funktion ```plt.scatter(x,y)``` ([Dokumentation](https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.scatter.html)) erlaubt es uns, einen einfachen Scatterplot zu erstellen:

# +
import matplotlib.pyplot as plt
# %matplotlib inline

x = [3,5,7,4]
y = [1,3,2,4]

plt.scatter(x,y)
# -

# **A.** Mache dich mit den Verschiedenen Argumenten der Funktion ```scatter()``` vertraut. Du kannst der Funktion z.B. mit dem Argument ```s``` eine Liste an Größen und mit dem Argument ```c``` eine Liste an Farben übergeben, um die Größe und Farbe der einzelnen dargestellten Punkte zu modifizieren.

# Wir laden den Fundstücke-Katalog und die Tabelle ```ÌCRATES_LOCATION.csv```, in der die Längen- und Breitengrade der verschiedenen Fundorte vermerkt sind:

import pandas as pd
catalogue = pd.read_csv("../daten/ICRATES/ICRATES_CATALOGUE.csv", encoding='ISO-8859-1')
location = pd.read_csv('../daten/ICRATES/ICRATES_LOCATION.csv', encoding = "ISO-8859-1", low_memory=False)
location.head()

# **B.** Wieviele verschiedene Fundorte gibt es? In welchen (modernen) Ländergrenzen liegen sie?

# **C.** Stelle die Fundorte als Scatterplot dar. In welchem Bereich (Längen- und Breitengrad) liegen die Datenpunkte? Was hat es mit dem Punkt bei (0,0) auf sich?

# Um herauszufinden, wieviele Fundstücke es pro Fundort gibt, müssen wir den Katalog nach der ```Location_ID``` filtern. Um z.B. die Anzahl der Fundstücke mit der ID 1 zu finden, gehen wir wie folgt vor:

# +
# nach dieser ID suchen wir
ID = 1

# wir wollen auch direkt den Namen des Fundortes herausfinden, dafür
# erstellen wir eine Maske 
name_mask = location['Location_ID'] == ID
# und filtern dann dass "location" DataFrame entsprechend
location_name = location[name_mask]['Location_simple'][0]

# um alle Fundstücke, die an einem bestimmten Fundort mit einer
# gegebenen ID gefunden wurden im Katalog zu finden, erstellen
# wir ebenfalls eine Maske
location_id_mask = catalogue['Location_ID'] == ID

# wir filtern den Katalog mit Hilfe der Maske...
filtered = catalogue[location_id_mask]
# ...und zählen die Fundstücke
count = len(filtered)

print('Am Fundort {} (ID {}) wurden {} Fundstücke gefunden.'\
      .format(location_name, ID, count))
# -

# **D.** Iteriere über alle Location IDs in der ```location``` Tabelle. Finde für jeden Fundort heraus, wieviele Fundstücke dort gefunden wurden. Speichere die Ergebnisse in einer neuen Liste, füge die Liste dem ```location``` DataFrame als neue Spalte hinzu.

# <a name="landkarten"></a>2. Landkarten
# ---
# Der in Aufgabe **1.C** erstellte Scatterplot der Fundorte ist schwer zu interpretieren. Insbesondere fehlt uns der Kontext der Geographie wie Ländergrenzen, Landmassen und Wasser. Um die Darstellung zu verbessern, ist es z.B. sinnvoll, im Hintergrund des Scatterplots eine Landkarte der Gegend darzustellen. 

# ### Exkurs zu Geopandas
# Um Landkarten darzustellen, gibt es in ```Python``` die spezialisierte Bibliothek ```geopandas```, mit der sehr einfach Landkarten dargestellt werden können. Da die Bibliothek auf dem JupyterHub noch nicht vorinstalliert ist, müssen wir sie erst einmal selbst herunterladen und installieren:

# +
# die nachfolgenden Kommandos musst du dir nicht merken. Falls du jemals eine
# eigene Bibliothek nachinstallieren willst, ersezte "geopandas" mit dem
# Namen der von dir benötigten Bibliothek

# die Bibliothek "sys" gibt uns Zugriff auf Funktionen des Betriebssystems
# mit dem Operator "!" führen wir Code nicht im Python-Interpreter sondern
# auf der Kommandozeile aus. 
import sys
!{sys.executable} -m pip install geopandas

# importiere das frisch installierte geopandas unter dem Kürzel "gpd"
import geopandas as gpd
# -

# Um Landkarten darstellen zu können, brauchen wir erst einmal Informationen über den Verlauf der Ländergrezen. Diese sind standardisiert in sog. "shape-files" abgelegt. shape-files sind vielfach öffentlich zum Download verfügbar, z.B. auf [dieser](https://gadm.org/download_country_v3.html) Website. Die shape-files für Deutschland haben wir im Ordner ```gadm36_DEU_sh``` im StudIP hochgeladen. Der Ordner enthält eine Reihe von verschiedenen Dateien, die geopandas benötigt, um das shape-file korrekt einzulesen. Informationen liegen außerdem in verschiedenen "Ebenen" vor:
# * Ebene 0 ist die Ländergrenze von Deutschland
# * Ebene 1 ist enthält außerdem die Grenzen der Bundesländer
# * Ebene 2 enthält die Grenzen der Landkreise
# * Ebene 3 enthält die Grenzen der Gemeinden
#
# Wast du die Informationen über die Grenzen heruntergeladen, kannst du es sie geopandas einlesen:

# +
# lies das shape-file ein
DEU_map = gpd.read_file('../daten/gadm36_DEU_shp/gadm36_DEU_1.shp')

# das shape-file bzw. die darin enthaltene Karte verhält sich wie ein DataFrame
DEU_map.head()
# -

# Wir können die Landkarte ganz einfach darstellen (dabei wird im Hintergrund auf die Funktionalität der Bibliothek ```matplotlib``` zurückgegriffen:

# Darstellen der Landkarte
DEU_map.plot()

# Um einen Punkt auf der Landkarte darzustellen, müssen wir uns das Achsen-Objekt, auf dem der aktuelle Plot dargestellt ist, von matplotlib geben lassen. Das machen wir mit der Funktion ```gca()``` (für "get current axis"). Mit diesem Achsen-Objekt können wir dann wie gewohlt die Funktion ```scatter()``` ausführen:

# Darstellen der Landkarte
DEU_map.plot()
# Achsenobjekt liefern lassen
ax = plt.gca()
# einen roten Punkt auf der Landkarte darstellen
ax.scatter(8, 51, color='red')

# Damit haben wir alle nötigen Zutaten, um die Fundorte der Fundstücke auf einer Landkarte darzustellen!

# **A.** Lade die shape-files von Griechenland herunter. Stelle die level 1 Karte (Bundesländer) wie oben für Deutschland gezeigt dar. Zeichne einen Punkt an der Position von Athen ein.

# **B.** Wir wollen im Folgenden nur die Fundorte betrachten, die in und um Griechenland liegen. Eine kurze Suche mit Google Maps zeigt  
#
# Ort | Beschreibung | Breitengrad | Längengrad
# --- | -------------| -----------| -----------  
# Othoni | westlichster Punkt | 39.851232 | 19.391932
# Trigono | nörtlichster Punkt | 41.746835 | 26.174643  
# Rodos | östlichster Punkt (Zypern ausgenommen) | 36.438004 | 28.241690
# Gavdos | südlichster Punkt | 34.803067 | 24.122037
#
# Filtere die Location-Tabelle, so dass nur die Orte innerhalb Griechenlands verbleiben (du  kannst davon ausgehen, dass die "Ländergrenzen" von Griechenland vom nördlichsten, südlichsten, östlichsten und westlichsten Punkt des Landes aufgespannt werden).

# **C.** Stelle alle Fundorte in Griechenland auf der Griechenland-Karte als Punkte dar.

# **D.** Als letzten Schritt skaliere nun die Größe der Punkte der Fundorte mit der Anzahl der gefundenen Fundstücke. Benutze dafür das Argument ```s``` der Funktion ```scatter()```, dem du eine Liste der Größen übergeben kannst. Dafür kannst du die in Aufgabe **1.D** erstellte Liste der Funstückanzahl je Fundort verwenden.

# **E.** An welchen Orten wurden die meisten Fundstücke gefunden? Woran kann das deiner Meinung nach liegen?

# **F. (optional)** Darstellen der Fundorte im gesamten Mittelmeerraum:
# * Finde heraus, in welchen 10 (modernen) Ländern die meisten Fundstücke gefunden wurden
# * Lade die shape-files dieser Länder herunter.
# * Stelle die Landkarten dieser Länder in einer Darstellung dar  <font color='green'>**HINWEIS:** Du kannst der Funktion ```plot()``` mit dem Argument ```ax=``` eine spezifische Achse mitgeben, auf der die Darstellung geplottet werden soll. </font>.
# * Stelle alle Fundorte in diesen Ländern mit Kreuzen dar.
