# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _Lizenz: Das folgende Lehrmaterial kann unter einer [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) Lizenz frei verwendet, verbreitet und modifiziert werden._   
#  _Authoren: Jana Lasser (jana.lasser@ds.mpg.de)_  
#  _Das Lehrmaterial wurde im Zuge des Projektes "Daten Lesen Lernen", gefördert vom Stifterverband und der Heinz Nixdorf Stiftung erstellt._

# # Übung 4
# ### Praktische Hinweise
# $\rightarrow$ Übungen sind dafür gedacht, dass ihr sie euch daheim anseht und versucht, ein paar Aufgaben selbst zu lösen. In den Tutorien könnt ihr euch weiter mit dem Lösungen der Aufgaben beschäftigen und dabei Hilfe von den Tutor*innen bekommen.  
#
# $\rightarrow$ Wenn ein Fehler auftritt:
# 1. Fehlermeldung _lesen_ und _verstehen_
# 2. Versuchen, selbst eine Lösung zu finden ($\rightarrow$ hier findet das Lernen statt!)
# 3. Das Problem googlen (Stichwort: Stackoverflow) oder den/die Nachbar*in fragen
# 4. Tutor*in fragen
#
# $\rightarrow$ Unter <font color='green'>**HINWEIS**</font> werden Hinweise gegeben, die für die Lösung der Aufgabe hilfreich sind und oftmals auch weiterführende Informationen zur Aufgabe liefern.
#
# $\rightarrow$ Mit **(Optional)** gekennzeichnete Übungsteile sind für besonders schnelle Menschen :-).

# <a name="top"></a>Inhalt
# ---
# * [Aufgaben](#aufgaben)
# * [Exkurs Bilder](#bilder)
# * [Exkurs Text](#text)

# <a name="aufgaben"></a> Funktionen, Bilder und Text
# ---
# Für die Abschnitte zu Bildern und Text gibt es kein Lehrvideo, deswegen gibt es nach den Aufgaben jeweils einen Exkurs mit einer kurzen Einführung in das Thema, die du dir ansehen solltest, bevor du die Aufgaben bearbeitest.
# 1. **Funktionen**
#   1. Schreibe eine Funktion, die zwei Zahlen als Argumente erhält und ihre Summe, Differenz, Produkt und Quotienten auf der Kommandozeile ausgibt. Teste die Funktion mit verschiedenen Zahlenpaaren.
#   2. Schreibe eine andere Funktion, die eine Liste mit Zahlen als ein einzelnes Argument erhält und die Summe der Quadrate der Zahlen zurückgibt. Teste die Funktion mit verschiedenen Listen, die Zahlen enthalten.
#   3. **(Optional)** Schreibe eine Funktion, die eine Liste mit Zahlen als Argument erhält, diese sortiert und dann zurückgibt (benutze hierfür nicht ```sort()```, sonst wäre es ja langweilig...). Teste deine Funktion ausgiebig, auch mit großen Listen. Was für Probleme können beim Sortieren von Listen entstehen, insbesondere wenn die Liste sehr lange ist?
#   
# 2. **Bilder**  
# <font color='green'>**HINWEIS:** Für den folgenden Abschnitt, lade bitte das Bild "cat_image.JPG" aus dem Ordner dieser Übung im StudIP herunter und in deinen Arbeitsbereich am Jupyter-Hub (am besten in einem eigenen Order "daten" hoch. </font>
#   1. Lade das Bild "cat_image.JPG" mit Hilfe der Bibliothek ```PIL```, transformiere es in mit ```NumPy``` in ein array und zeige es mit ```matplotlib.pyplot``` an (genau so wie unten im Exkurs beschrieben).
#   2. Zeige nur das Gesicht der Katze an, indem du den entsprechenden Bereich in dem array auswählst.
#   3. Speichere den Ausschnitt ab.
#   4. **(Optional)** Erstelle ein Graustufenbild, zeige es in Matplotlib (in Graustufen!) and und speichere es. [Hier](https://stackoverflow.com/questions/12201577/how-can-i-convert-an-rgb-image-into-grayscale-in-python) gibt es z.B. mehrere Beschreibungen, wie das in Python möglich ist. Wichtig: es gibt keine einzelne richtige Lösung sondern viele Wege zum Ziel.
#   
# 3. **Text**  
# <font color='green'>**HINWEIS:** Für den folgenden Abschnitt, lade bitte den Text "GG.txt" aus dem Ordner dieser Übung im StudIP herunter und in deinen Arbeitsbereich am Jupyter-Hub (am besten in einem eigenen Order "daten" hoch. Der Text enthält das Deutsche Grundgesetz. </font>
#
#   1. Lade den Text aus der Datei "GG.txt" (genauso wie unten im Exkurs beschrieben).
#   2. Finde heraus, wie oft das Wort "Freiheit" und das Wort "Gott" im Grundgesetzt vorkommt. Experimentiere mit anderen Wörtern, die dich interessieren.
#   3. Entferne die Zeichenkombination ```\n```, die für einen Zeilenumbruch steht aus dem Text. Wieviele Zeichen hat das Grundgesetz jetzt? <font color='green'>**HINWEIS**: um Werte in einer Liste dauerhaft zu verändern, musst du auf sie über ihren Index zugreifen. Dabei ist z.B. die Funktion ```enumerate()``` hilfreich [siehe Dokumentation](http://book.pythontips.com/en/latest/enumerate.html). Zur Illustration des Problems:</font>

# +
# erstelle eine Testliste
liste = ['a', 'b', 'c', 'd', 'f']
print('originale liste: {}'.format(liste))

# das verändert nichts, da immer nur der 
# Wert der temporären Variablen "eintrag"
# verändert wird
for eintrag in liste:
    eintrag = 'z'
    
print('erster Versuch: {}'.format(liste))

# das verändert die Einträge in der
# Liste nachhaltig
for index in range(len(liste)):
    liste[index] = 'z'
    
print('zweiter Versuch: {}'.format(liste))
# -

# 3. **Text Fortsetzung**  
#   4. Wie viele Leerzeichen hat das Grundgesezt und was ist die durchschnittliche Länge eines Wortes?
#   5. **(Optional)**: erstelle eine zusätzliche Spalte in der Tabelle, indem die durchschnittliche Wortlänge für jede Zeile angeführt wird und speichere die Tabelle als .csv-Datei.Was für Probleme fallen dir bei deinen Versuchen, eine durchschnittliche Wortlänge je Zeile auszurechenen auf? Wie verhalten sich leere Zeilen?
#   6. **(Optional)**: vergleiche die durchschnittliche Länge eines Wortes mit einem anderen Text, der dich interessiert. Ist sie größer oder kleiner? Wie kannst du dir das erklären?

# [Anfang](#top)

# <a name="bilder"></a> Exkurs Bilder
# ---
# Um Bilder zu öffnen benutzen wir das Untermodul ```Image``` der Bibliothek ```PIL``` (**P**ython **I**mage **L**ibrary, [Dokumentation](https://pillow.readthedocs.io/en/3.1.x/reference/Image.html)). Bilder anzeigen können wir mit der Funktion ```imshow()``` der Bibliothek ```matplotlib.pyplot``` ([Dokumentation](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.imshow.html)):

# +
# importiere "Image" aus der Bibliothek PIL
from PIL import Image

# importiere das Untermodul "pyplot" aus der
# Bibliothek Matplotlib mit dem Kürzel "plt"
import matplotlib.pyplot as plt

# um Bilder direkt im Dokument anzuzeigen,
# müssen wir dem Jupyter-Notebook noch folgenden
# Befel geben:
# %matplotlib inline

# lade das Bild mit der Funktion open()
# und speichere es in der Variable "bild"
# HINWEIS: dieses Kommando funktioniert nur, wenn
# das Bild im Ordner "daten" liegt. Sollte
# das Bild im selben Ordner wie dieses Dokument
# liegen, ist das alternative Kommando
# bild = Image.open('cat_image.JPG')
bild = Image.open('daten/cat_image.JPG')

# zeige das Bild an
plt.imshow(bild)
# -

# Wir können uns einfach einen Überblick über die in dem Bild enthaltene Information machen. Digitale Bilder bestehen aus einzelnen Punkten (Pixel), die jeweils drei Farbwerte (rot, grün und blau) besitzen. Diese Punkte sind in zwei Dimensionen neben und untereinander angeordnet und bilden eine sogenannte Matrix (in Python heißt das "array"). 
#
# Um das von uns geladene Bild in ein einfach zu handhabendes array zu verwandeln, verwenden wir die Funktion ```asarray()``` der Bibliothek ```NumPy``` ([Dokumentation](https://docs.scipy.org/doc/numpy/reference/generated/numpy.asarray.html)):

# +
# importiere die Bibliothek numpy mit dem
# Kürzel "np"
import numpy as np

# verwandle das Bild von vorhin in ein array
bild = np.asarray(bild)
# -

# Numpy arrays haben eine Menge eingebauter Funktionen (siehe [hier](https://docs.scipy.org/doc/numpy/reference/generated/numpy.array.html)), mit deren Hilfe wir uns einen guten ersten Überblick über die im Bild enthaltene Information machen können:

# "shape" gibtuns Informationen über die
# Anzahl der Pixel entlang jeder Dimension
# des Bildes. Dieses Bild ist 3632 Punkte hoch,
# 5456 Punkte breit und drei Punkte "tief". Hinter
# der Tiefe des Bildes verbergen sich die drei
# Farbkanäle des bunten Bildes (rot, grün, blau)
print(bild.shape)

# Farbwerte in digitalen Bildern können Werte
# zwischen 0 und 254 annehmen. Wir können uns
# einfach ansehen, wie viele Farbwerte das Bild
# besitzt
print('maximaler Farbwert: {}'.format(bild.max()))
print('minimaler Farbwert: {}'.format(bild.min()))
print('mittlerer Farbwert: {}'.format(bild.mean()))

# Außerdem ist es mit der Darstellung als array auch sehr einfach, nur Teile des Bildes anzuzeigen:

# zeige nur die obere Hälfte des Bildes an
plt.imshow(bild[0:1500,0:,0:])

# zeige nur einen Farbkanal an
plt.imshow(bild[0:,0:,0])

# Schlussendlich können wir das erzeugte Bild mit der Funktion ```imsave()``` ([Dokumentation](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.imsave.html)) speichern.

# +
# zeige das Bild an
plt.imshow(bild[0:,0:,0])

# speichere das Bild unter dem Namen "katze.png"
plt.savefig('daten/katze.png')
# -

# [Anfang](#top)

# <a name="text"></a> Exkurs Text 
# ---
# Um Text zu laden, können wir die in Python eingebaute Funktion ```open()``` verwenden ([Dokumentation](https://docs.python.org/3/library/functions.html#open)). Das liefert erst einmal ein sogenanntes file-handle zurück, das auf die Datei "zeigt". Um auf den in der Datei enthaltenen Text zugreifen zu können, müssen wir noch die Funktion ```readlines()``` ([Dokumentation](https://docs.python.org/2.4/lib/bltin-file-objects.html)) bemühen, die den Text zeilenweise "liest" und eine Liste von Strings (einer für jede Zeile) zurückgibt.

text_file = open('daten/GG.txt')
text = text_file.readlines()

print(text)
# HINWEIS: die Zeichenkombination "\n" erzeugt
# einen Zeilenumbruch und ist entsprechend oft
# im Text zu finden

# Die Anzahl der Zeilen lässt sich sehr einfach herausfinden

len(text)

# Ebenso die Anzahl der Zeichen pro Zeile

zeichen = [len(zeile) for zeile in text]
print(zeichen)

# Und die insgesamte Anzahl an Zeichen

print('Das deutsche Grundgesetz hat {} Zeichen.'\
      .format(sum(zeichen)))

# Zu guter Letzt wollen wir noch die Anzahl der Artikel im Grundgesetz herausfinden. Das machen wir mit Hilfe der Funktion ```count()``` 

artikel = [zeile.count('Art') for zeile in text]
sum(artikel)

# Um das Grundgesetz für die Zukunft in einer Form vorliegen haben, die einfacher zu handhaben ist, speichern wir es zeilenweise als Tabelle in einem Pandas ```DataFrame``` und exportieren es dann mittels der Funktion ```to_csv()``` ([Dokumentation](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_csv.html)), die das DataFrame bereitstellt als .csv-Datei:

# +
# importiere die Bibliothek "Pandas" unter
# dem Kürzel "pd"
import pandas as pd

# erstelle ein neues DataFrame mit den Zeilen des
# Grundgesetzes in einer Spalte und der Zeichenanzahl
# in der zweiten Spalte
tabelle = pd.DataFrame({'zeilen':text, 'zeichen':zeichen})
# -

# zeige die ersten Zeilen des DataFrame an
tabelle.head()

# greife auf die Spalte mit dem Namen 'zeile'zu
tabelle['zeilen']

# speichere das DataFrame
tabelle.to_csv('daten/grundgesetz.csv')
