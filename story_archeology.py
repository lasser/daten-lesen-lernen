# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
<<<<<<< HEAD
#       jupytext_version: 1.1.0
=======
#       jupytext_version: 1.1.1
>>>>>>> removed old ipynbs
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import pandas as pd
import matplotlib.pyplot as plt
from os import getcwd
from os.path import join

# Erklärung hier: https://archaeologydataservice.ac.uk/archives/view/icrates_lt_2018/overview.cfm
#
# ![Bild](https://archaeologydataservice.ac.uk/archives/view/icrates_lt_2018/images/ICRATES_ER.png)

# +
# adaptive file I/O
path = getcwd()
path = join(path, "daten/ICRATES")

catalogue = pd.read_csv(join(path,'ICRATES_CATALOGUE.csv'), encoding = "ISO-8859-1", low_memory=False)
location = pd.read_csv(join(path,'ICRATES_LOCATION.csv'), encoding = "ISO-8859-1", low_memory=False)
standard_form = pd.read_csv(join(path,'ICRATES_STANDARD_FORM.csv'), encoding = "ISO-8859-1", low_memory=False)
publication = pd.read_csv(join(path,'ICRATES_PUBLICATION.csv'), encoding = "ISO-8859-1", low_memory=False)
deposit = pd.read_csv(join(path,'ICRATES_DEPOSIT.csv'), encoding = "ISO-8859-1", low_memory=False)
# -

catalogue.head()

# ## Sherd yield per location
# Question: how many sherds did they find at every location?

location.head()

# first check: is the number of shards already recorded in the location sheet?
# no, it's not
location.columns

# Now: add a column with the sherd count per location

# +
sherd_count = []
for ID in location['Location_ID']:
    location_id_mask = catalogue['Location_ID'] == ID
    filtered = catalogue[location_id_mask]
    count = len(filtered)
    sherd_count.append(count)
    
location['Sherd_Count'] = sherd_count
# -

plt.hist(location['Sherd_Count'],bins=50)

# What do we see in this histogram?
# * One location yielded a huge number of sherds (>4000), a second location yielded >2000 sherds, all other locations yielded <1500 sherds and the majority yielded <100 sherds.
# * The distribution is not visualized nicely because there are a lot of empty bins between the ~100 sherds region and the 4000+ sherds region.
#
# Questions:
# * What is the [mean](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.mean.html) and [median](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.median.html) number of sherds per location? What is the [mode](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.mode.html)? Which of those measures describes the distribution of sherds per location best and why?
# * Improve the visualization in the histogram by using the ```range=``` keyword argument and the ```bins=``` keyword argument
# * Which are the names and locations of the two sites that yielded the most sherds? Can you explain, why they yielded so many more sherds than the other locations?
# * **Optional:** Are there locations, in which zero sherds were found? If yes, which locations are those and why do you think they are listed in the location database?

# +
mean = location['Sherd_Count'].mean()
median = location['Sherd_Count'].median()
mode = location['Sherd_Count'].mode()[0]

print('the mean number of sherds per location is {:1.2f}'.format(mean))
print('the median number of sherds per location is {}'.format(median))
print('in most locations {} sherd(s) was/were found'.format(mode))
# -

plt.hist(location['Sherd_Count'],bins=20,range=[0,200]);

# find the index of the location with the most sherds
maximum_sherd_number = location['Sherd_Count'].max()
maximum_filter = location['Sherd_Count'] == maximum_sherd_number
maximum_index = location[maximum_filter].index[0]
print(location.loc[maximum_index])

# +
# find the second largest number of sherds
count = list(location['Sherd_Count'])
count.sort()
second_largest = count[-2]

# find the index of the second largest number of sherds
second_largest_filter = location['Sherd_Count'] == second_largest
second_largest_index = location[second_largest_filter].index[0]
print(location.loc[second_largest_index])
# -

# ## From which time are the sherds?

standard_form.set_index('Standard_Form_ID', inplace=True)
standard_form.head()

time = range(-300,800,50)
counts, bin_borders, patches = plt.hist(standard_form.Standard_Typo_chronological_Lower_Date.dropna(), bins=time)

len(standard_form.Standard_Typo_chronological_Upper_Date.dropna())

import numpy as np
len(catalogue['Standard_Form_ID'].dropna())

pd.isna(catalogue.loc[474]['Standard_Form_ID'])

len(catalogue)

catalogue = pd.read_csv("daten/ICRATES/ICRATES_CATALOGUE.csv", encoding='ISO-8859-1')
standard_form = pd.read_csv('daten/ICRATES/ICRATES_STANDARD_FORM.csv', encoding = "ISO-8859-1", low_memory=False)
print(len(catalogue))
print(len(standard_form))

catalogue = pd.read_csv("ICRATES_CATALOGUE.csv", encoding='ISO-8859-1')
standard_form = pd.read_csv('ICRATES_STANDARD_FORM.csv', encoding = "ISO-8859-1", low_memory=False)
print(len(catalogue))
print(len(standard_form))

standard_form.Standard_Form_ID.max()

# +
upper_dates = []
lower_dates = []

for i, row in catalogue.iterrows():
    ID = row["Standard_Form_ID"]
    if pd.isna(ID):
        upper_dates.append(np.nan)
        lower_dates.append(np.nan)
    else:
        time_upper = standard_form.loc[ID]['Standard_Typo_chronological_Upper_Date']
        time_lower = standard_form.loc[ID]['Standard_Typo_chronological_Lower_Date']
        upper_dates.append(time_upper)
        lower_dates.append(time_lower)
# -

catalogue['Upper_Date'] = upper_dates
catalogue['Lower_Date'] = lower_dates

time = range(-300,800,50)
counts, bin_borders, patches = plt.hist([catalogue['Upper_Date'].dropna(),catalogue['Lower_Date'].dropna()], bins=time)

# ## Visualization of site locations
# Now we want to plot a map with the sherd yield per location

# +
plt.scatter(locations_greece['Longitude'], locations_greece['Latitude'],\
          color='red')

for index, row in locations_greece.iterrows(): 
    plt.text(row['Longitude'], row['Latitude'],\
        row['Location_simple'], fontsize=4)
# -

import geopandas as gpd

# +
# shapefile source https://gadm.org/download_country_v3.html
# read shapefile
path = "/home/jana/DaLeLe/uebungen/daten/archaeology/gadm36_GRC_shp/gadm36_GRC_2.shp"
path_de = "/home/jana/DaLeLe/uebungen/daten/archaeology/gadm36_DEU_shp/gadm36_DEU_2.shp"
greece_map = gpd.read_file(path)
#greece_map.head()

DE_map = gpd.read_file(path_de)
DE_map.head()
# -

fig, ax = plt.subplots()
DE_map.plot(ax = ax)
ax.scatter(10, 55, color='red')

fig, ax = plt.subplots()
greece_map.plot(ax=ax)
ax.scatter(22, 40, color='red') # plot a red dot on the map

# Now we want to plot all the sites which are in Greece. For that, we filter the sites by therei coordinates.

# +
# latitude: Höhe
# longitude: Breite

# +
filter_greece = (location['Longitude'] > 20) & \
                (location['Longitude'] < 30) & \
                (location['Latitude'] > 33) & \
                (location['Latitude'] < 42)

locations_greece = location[filter_greece]
# -

locations_greece.head()

fig, ax = plt.subplots()
greece_map.plot(ax=ax)
ax.scatter(locations_greece['Longitude'], locations_greece['Latitude'],\
          color='red')

# +
import numpy as np

fig, ax = plt.subplots()
greece_map.plot(ax=ax)
ax.scatter(locations_greece['Longitude'], locations_greece['Latitude'],\
          s=locations_greece['Sherd_Count'],color='red',alpha=0.3,\
          label='sherd count')
ax.set_xlabel('Longitude')
ax.set_ylabel('Latitude')
ax.set_title('Number of Sherds found in Greece')
# -

# ### Questions
# * Which other countries were major sources of the sherds in this dataset?
# * Visualize Turkey in the same way as Greece. The shapefile of the country boarders can be found for example [here](https://gadm.org/download_country_v3.html).
# * **Optional:** implement a function which, for a given set of longitude and latitude coordinates, finds out, whether a location lies within the borders of a modern country. I.e. for example is the location E 24 N 38 within Greece?

city_df = pd.DataFrame()
names = ['glyfada', 'athens', 'argos', 'nikopolis', 'thasos']
inhabitants = [87305, 664046, 22209, 31733, 3240, ]

locations_greece['Location_simple']

# ## Which form do the sherds have?

catalogue.head(2)

print('sherd form categories: {}'.format(catalogue.General_Shape.unique()))
op = sum(catalogue.General_Shape == 'Open')
clo = sum(catalogue.General_Shape == 'Closed')
no_info = len(catalogue) - op - clo

# visualize as barchart
plt.bar(['open', 'closed', 'no information'], [op, clo, no_info])
plt.ylabel('count')

# visualize as barchart but with percentages
tot = len(catalogue)/100
plt.bar(['open', 'closed', 'no information'], \
        [op/tot, clo/tot, no_info/tot])
plt.ylabel('%')

# ## How many sherds have time information?

standard_form.head(2)

# +
# use attribute "Standard_Typo_chronological_Lower_Date" from
# standard-form table
lower_date = []
upper_date = []

import numpy as np

for i in range(len(catalogue)):
    ID = catalogue.loc[i]['Standard_Form_ID']
    ID_index = standard_form.index[standard_form['Standard_Form_ID'] == ID].tolist()
    lower = standard_form.loc[ID_index]['Standard_Typo_chronological_Lower_Date']
    upper = standard_form.loc[ID_index]['Standard_Typo_chronological_Upper_Date']
    
    if len(list(lower)) < 1:
        lower_date.append(np.nan)
    else:
        lower_date.append(lower)
        
    if len(list(upper)) < 1:
        upper_date.append(np.nan)
    else:
        upper_date.append(upper)
# -

lower_date

for entry in lower_date:
    if len(list(entry)) < 1:
        print(entry)
#lower_date = [list(entry)[0] for entry in lower_date]

len(lower_date)
catalogue['lower_date'] = lower_date
catalogue['upper_date'] = upper_date

# +
# how many sherds have info on their age?
chrono_info = (~catalogue['lower_date'].isna()) | \
              (~catalogue['upper_date'].isna())

catalogue['upper_date'].loc[6]
# -

# ## How do archeologists publish?

publication.head()

# [Documentation](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.dropna.html) of ```dropna()```.  
# [Helpful Stackoverflow post](https://stackoverflow.com/questions/20656663/matplotlib-pandas-error-using-histogram) found by googling the ```ValueError```.

# what happens, if we do not remove the NaNs before plotting?
year = publication['Year_of_Publication'].dropna()
plt.hist(year, bins=50);

deposit.head()


# +
def lookup_site_type1(Deposit_ID):
    match = (deposit['Deposit_ID'] == Deposit_ID)
    site_type1 = deposit['Site_type_1'][match]
    
    if len(site_type1.values) < 0:
        return site_type1.values[0]
    else:
        return np.nan

appl = catalogue['Deposit_ID'].apply(lookup_site_type1)
# -

appl.sum()

# From which site type doe the sherds come from?

len(deposit)

deposit.Site_type_1.unique()

deposit.Site_type_2.unique()

locations = len(data.Location_specific.unique())
datapoints = len(data)
print('number of unique locations: {}'.format(locations))
print('number of sherds in the database: {}'.format(datapoints))
print('number of sherds per location: {}'.format(datapoints / locations))

# +
catalogue.loc[12000:12005]['Deposit_ID']

for ID in catalogue.loc[11000:11005]['Deposit_ID']:
    match = (deposit['Deposit_ID'] == ID)
    site_type1 = deposit['Site_type_1'][match]
    print(ID)
    #print(site_type1)
# -



# ## Bonus

import os 
os.environ['PROJ_LIB'] = '/home/mav/anaconda3/envs/py3/share/proj/'
from mpl_toolkits.basemap import Basemap

Lat = inhalte['ICRATES_LOCATION'].Latitude.values
Lon = inhalte['ICRATES_LOCATION'].Longitude.values

# +
# miller projection
plt.figure(figsize=(15,15))

mymap = Basemap(projection='mill',lon_0=0)
# plot coastlines, draw label meridians and parallels.
mymap.drawcoastlines()

mymap.drawparallels(np.arange(-90,90,30),labels=[1,0,0,0])
mymap.drawmeridians(np.arange(mymap.lonmin,mymap.lonmax+30,60),labels=[0,0,0,1])
# fill continents 'coral' (with zorder=0), color wet areas 'aqua'
mymap.drawmapboundary(fill_color='lightblue')
mymap.fillcontinents(color='grey',lake_color='lightblue')

mymap.scatter(Lat,Lon, 100, marker='+',color='red', latlon=True)

plt.title('mymap of Locations')
plt.show()

# +
# miller projection
plt.figure(figsize=(15,15))

mymap = Basemap(llcrnrlon=5.,
                urcrnrlon=50.,
                llcrnrlat=15.,
                urcrnrlat=45.,
            projection='lcc',lat_1=0.,lat_2=10.,lon_0=-40.,
            resolution ='l',area_thresh=1000.)


# plot coastlines, draw label meridians and parallels.
mymap.drawcoastlines()

mymap.drawparallels(np.arange(-90,90,30),labels=[1,0,0,0])
mymap.drawmeridians(np.arange(mymap.lonmin,mymap.lonmax+30,60),labels=[0,0,0,1])
# fill continents 'coral' (with zorder=0), color wet areas 'aqua'
mymap.drawmapboundary(fill_color='lightblue')
mymap.fillcontinents(color='grey',lake_color='lightblue')

mymap.scatter(Lat,Lon, 100, marker='+',color='red', latlon=True)

plt.title('mymap of Locations')
plt.show()
# -

# [Anfang](#top)
