# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _This teaching material can be freely used, distributed and modified as per the [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) license._   
#  _Authors: Jana Lasser (jana.lasser@ds.mpg.de), Debsankha Manik (debsankha.manik@ds.mpg.de)._  
#  _This teaching material is created for the project "Daten Lesen Lernen", funded by Stifterverband and the Heinz Nixdorf Stiftung._

# # Solutions to Exercise 05 - General: reading data, cleaning data and visualizing with histograms

# <a name="top"></a>Contents
# ---
#
# * [Obtaining data](#daten_beschaffen)
# * [Exploring data](#daten_erkunden)
# * [Cleaning data](#daten_bereinigen)
# * [Histograms](#histogramme)

# <a name="daten_beschaffen"></a>1. Daten beschaffen
# ---
#
# **1.A** data sources:
# * ["normal" Twitter Users](http://help.sentiment140.com/for-students/) 
# This dataset was actually compiled for a so-called "sentiment analysis" of the tweets. In addition, the dataset is not publushed under a free license. That's why we couldn't include the data in this course's materials, but you had to download it yourself. The methods taken for compiling the dataset are described in [this publication](https://cs.stanford.edu/people/alecmgo/papers/TwitterDistantSupervision09.pdf).
#
# * [Donald Trump](https://github.com/mkearney/trumptweets) The data source for the Trump tweets are not well documented. This means that the data source is not necessarily trustworthy and we have to be careful. An alternative would be to download the tweets ourselves.
#
# * [Russian trolls](https://github.com/fivethirtyeight/russian-troll-tweets/) A very good description of the dataset as well as the method to identify the trolls are [on the GitHub page](https://github.com/fivethirtyeight/russian-troll-tweets/) of the project. You can [read this](https://fivethirtyeight.com/features/why-were-sharing-3-million-russian-troll-tweets/) article for a deeper understanding of the dataset.

# !ls data/

# +
# 1.B
# necessary modules
import pandas as pd 
import requests 
import io

# the path where we saved the downloaded "normal" tweets dataset
path_normal = 'data/training.1600000.processed.noemoticon.csv'
# now read the dataset into a dataframe. encoding is imprtant...
tweets_normal_raw = pd.read_csv(path_normal, \
                            names=['sentiment','ID','Date','Query','User','Tweet'],\
                            encoding="ISO-8859-1")

# the URL for the trump dataset
url_trump = "https://github.com/mkearney/trumptweets/raw/master/data/trumptweets-1515775693.tweets.csv"

# read the trump tweet dataset. encoding is imprtant...
tweets_trump_raw = pd.read_csv(url_trump, encoding="ISO-8859-1")

# the URL for the troll tweet dataset
url_troll = 'https://github.com/fivethirtyeight/russian-troll-tweets/raw/master/IRAhandle_tweets_1.csv'
# the rest is as before
tweets_troll_raw = pd.read_csv(url_troll, encoding="ISO-8859-1")
# -

# [Top](#top)

# <a name="daten_beschaffen"></a>2. Exploring data
# ---

# +
# 2.A
print('column names of normal Tweets:')
print(tweets_normal_raw.columns)
print()

print('column names of Trump Tweets:')
print(tweets_trump_raw.columns)
print()

print('column names of troll Tweets:')
print(tweets_troll_raw.columns)
# -

# **What does each column of the normal tweet dataset mean?** (from the [documentation](http://help.sentiment140.com/for-students/) of the dataset):
#
# _The data is a CSV with emoticons removed. Data file format has 6 fields:_
# * _0 - the polarity of the tweet (0 = negative, 2 = neutral, 4 = positive)_
# * _1 - the id of the tweet (2087)_
# * _2 - the date of the tweet (Sat May 16 23:58:44 UTC 2009)_
# * *3 - the query (lyx). If there is no query, then this value is NO_QUERY.*
# * _4 - the user that tweeted (robotickilldozr)_
# * _5 - the text of the tweet (Lyx is cool)_

# **What does each column of the troll tweet dataset mean?** (from the [documentation](https://github.com/fivethirtyeight/russian-troll-tweets/blob/master/README.md)  of the dataset):
#
# Header | Definition
# ---|---------
# `external_author_id` | An author account ID from Twitter 
# `author` | The handle sending the tweet
# `content` | The text of the tweet
# `region` | A region classification, as [determined by Social Studio](https://help.salesforce.com/articleView?id=000199367&type=1)
# `language` | The language of the tweet
# `publish_date` | The date and time the tweet was sent
# `harvested_date` | The date and time the tweet was collected by Social Studio
# `following` | The number of accounts the handle was following at the time of the tweet
# `followers` | The number of followers the handle had at the time of the tweet
# `updates` | The number of “update actions” on the account that authored the tweet, including tweets, retweets and likes
# `post_type` | Indicates if the tweet was a retweet or a quote-tweet
# `account_type` | Specific account theme, as coded by Linvill and Warren
# `retweet` | A binary indicator of whether or not the tweet is a retweet
# `account_category` | General account theme, as coded by Linvill and Warren
# `new_june_2018` | A binary indicator of whether the handle was newly listed in June 2018
# `alt_external_id` | Reconstruction of author account ID from Twitter, derived from `article_url` variable and the first list provided to Congress
# `tweet_id` | Unique id assigned by twitter to each status update, derived from `article_url`
# `article_url` | Link to original tweet. Now redirects to "Account Suspended" page
# `tco1_step1` | First redirect for the first http(s)://t.co/ link in a tweet, if it exists
# `tco2_step1` | First redirect for the second http(s)://t.co/ link in a tweet, if it exists
# `tco3_step1` | First redirect for the third http(s)://t.co/ link in a tweet, if it exists

# There is no explicit description of the colmns in the Trump tweet dataset source. Nevertheness, most of the column names are in most cases so descriptive that we can guess what they contain.

# 2.B
test_index = 10
print('A random normal user tweets:')
print(tweets_normal_raw['Tweet'][test_index])
print()
print('Donald Trump tweets:')
print(tweets_trump_raw['text'][test_index])
print()
print('A random Russian Troll tweets:')
print(tweets_troll_raw['content'][test_index])

# +
# 2.C
# normal users
number_of_tweets_normal = len(tweets_normal_raw)
unique_users_normal = len(tweets_normal_raw.User.unique())
print('Number of normal users: {}'.format(unique_users_normal))
print('Number of tweets in the normal tweets dataset: {}'.format(number_of_tweets_normal))
print()

# Trump
number_of_tweets_trump = len(tweets_trump_raw)
unique_users_trump = len(tweets_trump_raw.screen_name.unique())
print('Number of users in the Trump tweets: {}'.format(unique_users_trump))
print('umber of tweets in the trump tweets dataset: {}'.format(number_of_tweets_trump))

# +
# One would expect only one username in the Trump tweet dataset (i.e. Trump).
# apparently there are 5 users. What are they?

print(tweets_trump_raw.screen_name.unique())

# +
# 2.D
# Trolls
number_of_tweets_troll = len(tweets_troll_raw)
unique_users_troll = len(tweets_troll_raw.author.unique())

# now compute tweet per user

print('Number of Tweets / User for the normal tweets: {:1.2f}'.format(number_of_tweets_normal / unique_users_normal))
print('Number of Tweets / User for the trolls: {:1.2f}'.format(number_of_tweets_troll / unique_users_troll))
# -

# 2.E
# dhe "mode" is the entry that appears most numner of times
most_active_user = tweets_troll_raw['author'].mode()
print('The most active troll is:')
print(most_active_user)
print()

# Filter only the tweets from "AMELIEBALDWIN"
filter_most_active = tweets_troll_raw['author'] == 'AMELIEBALDWIN'
number_most_active = filter_most_active.sum()
print('The most active troll has written {} Tweets.'.format(number_most_active))
print('That is  {:1.2f}% of all the tweets in the dataset.'\
      .format(number_most_active / len(tweets_troll_raw) * 100))

# +
# 2.F
# the different entries in teh column "language"
languages = tweets_troll_raw.language.unique()
print(languages)
print()

# we need to subtract 1 for the 'LANGUAGE UNDEFINED'
print('The dataset contains tweets in {} different languages.'\
     .format(len(languages) - 1))

# +
# 2.F continued

# create a dictionary with an entry for each language and set the
# counter (for counting teh number of tweets in that language) to 0

tweet_per_language = {l:0 for l in languages}

# now we iterate oer the DataFrame, check what language the 
# tweet is, and add 1 to teh entry in teh dictioanry 
for index, row in tweets_troll_raw.iterrows():
    # find the tweet language
    tweet_language = row['language']
    # increase the counter in the dictionary to 1
    tweet_per_language[tweet_language] \
        = tweet_per_language[tweet_language] + 1 
# -

tweet_per_language

# [Top](#top)

# <a name="daten_bereinigen"></a>3. Cleaning data
# ---

# +
# 3.A
# clean the Trump Tweets (repeating from the exercise notebook)
mask_realDonald = tweets_trump_raw['screen_name'] == 'realDonaldTrump'
tweets_trump_filtered = tweets_trump_raw[mask_realDonald]
tweets_trump = pd.DataFrame({'Date':tweets_trump_filtered['created_at'],\
                             'User':tweets_trump_filtered['screen_name'],\
                             'Tweet':tweets_trump_filtered['text']})

# normal users
tweets_normal = pd.DataFrame({'Date':tweets_normal_raw['Date'],\
                             'User':tweets_normal_raw['User'],\
                             'Tweet':tweets_normal_raw['Tweet']})
# -

# 3.B
# trolls
tweets_troll = pd.DataFrame({'Date':tweets_troll_raw['publish_date'],\
                             'User':tweets_troll_raw['author'],\
                             'Tweet':tweets_troll_raw['content'],\
                             'Language':tweets_troll_raw['language'],\
                             'Follower':tweets_troll_raw['followers'],\
                             'Following':tweets_troll_raw['following']})
tweets_troll.head()

# +
# 3.C

# URL of the troll tweets, with a "{}" as a placeholder
# for the serial number (1 to 13)
url_troll_base = 'https://github.com/fivethirtyeight/russian-troll-tweets/raw/master/IRAhandle_tweets_{}.csv'
# list in which we will store the downloaded troll tweets
all_troll_tweets_raw = []

# iterate over the numbers 1 to 13, to download all 13 parts of the dataset
for i in range(1, 14): # rememger range(1, n) stops at n-1
    url_troll = url_troll_base.format(i)
    tweets = pd.read_csv(url_troll, encoding="ISO-8859-1")
    # append this part of teh dataset to the list
    all_troll_tweets_raw.append(tweets)
    print('The  {}. part of the dataset contains{} Tweets.'.format(i, len(tweets)))
    
#The tweets are sorted alphabetically by username
# each part of teh dataset contain exactly 243891 tweets 
# -

# now concatenate all 13 parts into a big dataFrame
all_troll_tweets_raw = pd.concat(all_troll_tweets_raw)

# +
# clean the  DataFrame by deleting unnecessary columns
tweets_troll_all = pd.DataFrame({'Date':all_troll_tweets_raw['publish_date'],\
                             'User':all_troll_tweets_raw['author'],\
                             'Tweet':all_troll_tweets_raw['content'],\
                             'Language':all_troll_tweets_raw['language'],\
                             'Follower':all_troll_tweets_raw['followers'],\
                             'Following':all_troll_tweets_raw['following']})

print('The total dataset contains {} Tweets.'.format(len(tweets_troll_all)))
tweets_troll_all.head()

# +
# 3.D
# import the datetime module
import datetime

# define the function "clean_date", that accepts a sring (timestamp)
# containing the data and time information, as well as the 
# type (normal, trump or troll) of the input.
# this function will format the input string in a uniform format
def clean_date(date_time_raw, typ):
    # dictionary to convert a month abbreviation (3 character)
    # to a number (between 1 and 12)
    # we need this because in normal Tweet dataset we have the
    # months as three character strings
    month_dict = {'Jan':1, 'Feb':2, 'Mar':3, 'Apr':4,\
                 'May':5, 'Jun':6, 'Jul':7, 'Aug':8,\
                 'Sep':9, 'Oct':10, 'Nov':11, 'Dec':12}
    
    # check teh type of the input
    # in the troll dataset, the timestamp is of
    # month/day/year hour:minute format"
    if typ == 'troll':
        # split the string by whitespace in date and time part
        date, time = date_time_raw.split(' ')
        # split the string by whitespace in date and time partmonth, day and year
        month, day, year = date.split('/')
        # split the time by ':' to get hour and minute
        hour, minute = time.split(':')
    
    # timestamps of teh normal tweets follow the format
    # "weekday month day hour:minute:second timezone year"
    elif typ == 'normal':
        # Split by whitespace into weekday, month, day
        # time, timezone, year
        weekday, month, day, time, zone, year = date_time_raw.split(' ')
        # split the time by ":" into hour, minute and second
        hour, minute, second = time.split(':')
        # convert the month (as three character string) to a number
        # using month_dict
        month = month_dict[month]
    
    # Timestamps of the Trump dataset follow the
    # "year-month-day hour:minute:second" format
    elif typ == 'trump':
        # split the string by whitespace in date and time part
        date, time = date_time_raw.split(' ')
        # split the string by '-' into year, month and day
        year, month, day = date.split('-')
        # split the time by ":" into hour, minute and second
        hour, minute, second = time.split(':')
    
    # in case this function is supplied with a type othefr than
    # 'troll', 'trump' or 'normal', return None
    else:
        print('unknown Tweet type: {}'.format(typ))
        return None
    # we use year, month, day, hour and minute as integers
    # since the troll dataset doesn't have seconds, we omit
    # seconds for all typ's and return a value accurate only upto minutes
    
    year = int(year)
    month = int(month)
    day = int(day)
    hour = int(hour)
    minute = int(minute)
    
    # create a datatime object
    date_time = datetime.datetime(year, month, day, hour, minute)
    
    # return the datetime object
    return date_time        


# +
# test the function with all three datasets
trump_test = tweets_trump.loc[0]['Date']
normal_test = tweets_normal.loc[0]['Date']
troll_test = tweets_troll.loc[0]['Date']

clean_trump = clean_date(trump_test, 'trump')
print(clean_trump)
clean_normal = clean_date(normal_test, 'normal')
print(clean_normal)
clean_troll = clean_date(troll_test, 'troll')
print(clean_troll)
# -

# now convert teh datetime column in all three dataFrames with our
# cool function
tweets_trump['Date'] = tweets_trump['Date'].apply(clean_date, args=['trump'])
tweets_normal['Date'] = tweets_normal['Date'].apply(clean_date, args=['normal'])
tweets_troll['Date'] = tweets_troll['Date'].apply(clean_date, args=['troll'])

# +
# now we can perform various calculations with the datetimes
# of the tweets very easily. 

# the following returns a Timedelta object
tweets_trump.loc[0]['Date'] - tweets_trump.loc[4]['Date']


# +
# 3.E

# define the function "calculate_length", that accepts
# the dataframe (df) as input
def calculate_tweet_length(df):
    
    # list in which the tweet lengths will be computed
    length = []
    
    # iterate over all tweets
    for tweet in df['Tweet']:
        # append the length of this specific tweet
        length.append(len(tweet))

    # create a new column in the dataframe named
    # "tweet_length" and put the values in the 
    # list length in it
    df['tweet_length'] = length
    
# apply the function to all the dataframes
calculate_tweet_length(tweets_normal)
calculate_tweet_length(tweets_trump)
calculate_tweet_length(tweets_troll)


# +
# 3.F
def calculate_word_number(df):
    
    # list to store the word lengths
    word_number = []
    
    # iterate over all tweets
    for tweet in df['Tweet']:
        # we assume words are separated by shitespaces
        words = tweet.split(' ')
        word_number.append(len(words))

    # create a new column in the dataframe named
    # "word_number" and put the values in the 
    # list word_number in it
    df['word_number'] = word_number
    
calculate_word_number(tweets_normal)
calculate_word_number(tweets_trump)
calculate_word_number(tweets_troll)
# -

# the datasets should now have two additional columns "tweet_length"
# und "word_number"
tweets_normal.head()

# 3.G
tweets_normal.to_csv('data/tweets_normal.csv', sep='~',encoding='ISO-8859-1')
tweets_trump.to_csv('data/tweets_trump.csv', sep='~', encoding='ISO-8859-1')
tweets_troll.to_csv('data/tweets_troll.csv', sep='~', encoding='ISO-8859-1')
#tweets_troll_all.to_csv('data/tweets_troll_all.csv', sep='|', encoding='ISO-8859-1')

tweets_trump.to_csv('data/tweets_trump.csv', sep=':', encoding='ISO-8859-1')

# [Top](#top)

# <a name="daten_beschaffen"></a>4. Histograms
# ---

# 4.A
# import the submodule pyplot from module matplotlib
# for creating plots
import matplotlib.pyplot as plt
# this command instruct jupyter notebooks to display
# plots in teh browser itself
# %matplotlib inline

# +
# we need to find out how many followers each troll has and
# how many he/she is following

usernames = []
followers = []
followings = []

# iterate over each troll 
for user in tweets_troll['User'].unique():
    # get all rows for this specific user
    mask = tweets_troll['User'] == user
    tweet_user = tweets_troll[mask]
    # get average values for"Follower" and "Following"
    num_followers_avg = tweet_user['Follower'].mean()
    num_followings_avg = tweet_user['Following'].mean()
    
    usernames.append(user)
    followers.append(num_followers_avg)
    followings.append(num_followings_avg)

# put in a new DataFrame
follow_df = pd.DataFrame({"User" : usernames,
                          "Followers" : followers,
                          "Followings": followings})
follow_df.head()
# -

# create a histogram of the followers of the troll tweets
plt.hist(follow_df['Followers']);

# +
# 4.Ba

# The values of followers lie within 0 and 20000 Tweets. 
# By setting the `range` parameter (optional argument)
# of the plt.hist function to [0, 20000] and setting the\
# number of bins to 20, we make sure that the bins begin
# and end at an exact multiple of 1000, helping us interpret
# the histogram better. The new visualization makes it clear
# that most of the accounts have 0-3000 followers. Additionally
# now we see lot for details in the 0-3000 range, that were
# lost in the previous histogram, because all the accounts with
# followers between 0-2000 were aggregated together in one bin

# setting the `rwidth` to a value < 1.0 is also a good idea, since
# then we get a athin border between two successive bins
# Histogram
plt.hist(follow_df['Followers'], range=[0,20000], bins=20, rwidth=0.8)

# as always, axes labels and title
plt.xlabel('Number of followers')
plt.ylabel('Number of accounts')
plt.title('Number of follower for each troll account');

# +
# 4.Bb
# in order to inspect in detail the x range 0-3000, we make a 
# new histogram only in that range, and increase the number
# of bins

# Histogram
plt.hist(follow_df['Followers'], range=[0,3000], bins=30, rwidth=0.8)

#axes labels and title
plt.xlabel('Number of followers')
plt.ylabel('Number of accounts')
plt.title('Number of follower for each troll account');

# +
# 3.C Try having fewer bins
# this leads to loss of much information (as we saw up top)

# Histogram
plt.hist(follow_df['Followers'], range=[0,3000], bins=5, rwidth=0.8)

#axes labels and title
plt.xlabel('Number of followers')
plt.ylabel('Number of accounts')
plt.title('Number of follower for each troll account');

# +
# 3.C much more bins
# Here we lose the advantage of aggregating information. We have
# much more detailed information, but it's difficult to extract
# a general insight about the data

# Histogram
plt.hist(follow_df['Followers'], range=[0,3000], bins=1000, rwidth=0.8)

#axes labels and title
plt.xlabel('Number of followers')
plt.ylabel('Number of accounts')
plt.title('Number of follower for each troll account');

# a good value for the range 0-5000 is (as we saw in 3.B) e.g. bins=50

# +
# 3.D

# In order to compare the "Following" and "Followers" histograms,
# it is highly recommended to set the range and bins of both
# histograms manually to the same set of values. 

# In order to increase the readability of the plot, we set the
# `alpha` value (transparency of the bins) to 50% - this way
# the longer bar from one plot cannot hide the shorter bar from
# the other plot. 


# HINT: With the argument "label", we give each histogram a name,
# which will get displayed when we call teh function plt.legend()
# otherwise we won't know which histogram is for which quantity

# Histogram of "Follower"
plt.hist(follow_df['Followers'], range=[0,3000], bins=15, rwidth=0.8, \
         alpha=0.5, label='Follower')

# Histogram of "Following"
plt.hist(follow_df['Followings'], range=[0,3000], bins=15, rwidth=0.8, \
         alpha=0.5, label='Following')

# axes labels and title
plt.xlabel('Number of following/follower accounts')
plt.ylabel('Number of accounts')
plt.title('Following numbers of the troll accounts');

# Legend
plt.legend();

# The distribution of following and follower accounts of the trolls
# seem quite similar: many in low 100's, relatively fewer in high
# thousands. We again set the range in order to focus on 0-3000.
# A slight trend emerges: trolls seem to follow more people than
# they have followers. 

# +
# 3.E (density)
# the argument "density" alters what the histogram shows in the y-axis. 
# Previously, the absolute counts (in each bin) were shown, with 
# `density`, we get the normalized counts, that can be interpreted
# as probability densities

# Histogram of Follower
plt.hist(follow_df['Followers'], range=[0,3000], bins=15, rwidth=0.8, \
         alpha=0.5, label='Follower', density=True)

# Histogram of Following
plt.hist(follow_df['Followings'], range=[0,3000], bins=15, rwidth=0.8, \
         alpha=0.5, label='Following', density=True)

# axes labels and title
plt.xlabel('Number of following/follower accounts')
plt.ylabel('Number of accounts')
plt.title('Following probabilities of the troll accounts');

# Legende
plt.legend();

# +
# 3.E (cumulative)
# if cumulative=True, is set, then each bin does not show the count
# of observation in the bin, but the number of observation in that 
# bin PLUS the total number of observations in all the previous bins
# Naturally, the value at the last bin the total number of
# observations. 

# Histogram of Follower
plt.hist(follow_df['Followers'], cumulative=True, range=[0,3000], bins=15, rwidth=0.8, \
         alpha=0.5, label='Follower')

# Histogram of Following
plt.hist(follow_df['Followings'], cumulative=True, range=[0,3000], bins=15, rwidth=0.8, \
         alpha=0.5, label='Following')

# axes labels and title
plt.xlabel('Number of following/follower accounts')
plt.ylabel('Number of accounts')
plt.title('Cumulative following numbers of the troll accounts');

# Legende
plt.legend();

# +
# 3.E (density & cumulative)
# if `density=True` is passed as well as `cumulative=True`, then the probability
# densities are normalized to 1. This means the cumulative probability of finding an
# observation in the last bin is always 1 

# Histogram of Follower
plt.hist(follow_df['Followers'], cumulative=True, density=True, range=[0,3000], bins=15, rwidth=0.8, \
         alpha=0.5, label='Follower')

# Histogram of Following
plt.hist(follow_df['Followings'], cumulative=True, density=True, range=[0,3000], bins=15, rwidth=0.8, \
         alpha=0.5, label='Following')

# axes labels and title
plt.xlabel('Number of following/follower accounts')
plt.ylabel('Number of accounts')
plt.title('Cumulative following densities\nof the troll accounts');

# Legende
plt.legend();
# -

# [Top](#top)


