# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _Lizenz: Das folgende Lehrmaterial kann unter einer [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) Lizenz frei verwendet, verbreitet und modifiziert werden._   
#  _Authoren: Jana Lasser (jana.lasser@ds.mpg.de)_  
#  _Das Lehrmaterial wurde im Zuge des Projektes "Daten Lesen Lernen", gefördert vom Stifterverband und der Heinz Nixdorf Stiftung erstellt._

# # Übung 2
# ### Praktische Hinweise
# $\rightarrow$ Übungen sind dafür gedacht, dass ihr sie euch daheim anseht und versucht, ein paar Aufgaben selbst zu lösen. In den Tutorien könnt ihr euch weiter mit dem Lösungen der Aufgaben beschäftigen und dabei Hilfe von den Tutor*innen bekommen.  
#
# $\rightarrow$ Wenn ein Fehler auftritt:
# 1. Fehlermeldung _lesen_ und _verstehen_
# 2. Versuchen, selbst eine Lösung zu finden ($\rightarrow$ hier findet das Lernen statt!)
# 3. Das Problem googlen (Stichwort: Stackoverflow) oder den/die Nachbar*in fragen
# 4. Tutor*in fragen
#
# $\rightarrow$ Unter <font color='green'>**HINWEIS**</font> werden Hinweise gegeben, die für die Lösung der Aufgabe hilfreich sind und oftmals auch weiterführende Informationen zur Aufgabe liefern.
#
# $\rightarrow$ Mit **(Optional)** gekennzeichnete Übungsteile sind für besonders schnelle Menschen :-).

# ### Listen, Loops und Tabellen
# 1. **Listen und Loops**  
#   1. Erstelle eine Liste mit Worten (Strings) als Elementen, die zusammen einen Satz bilden.
#   2. Gib jedes Wort einmal aus, indem du mit einem ```for``` loop über die Liste iterierst.
#   3. Addiere alle Strings zu einem einzigen String indem du mit einem ```for``` loop über die Liste iterieren.  
#   <font color='green'>**HINWEIS**: Du solltest vor Beginn des loops eine neue Variable definieren, in der der neue String gespeichert wird.</font>
#   4. Erstelle eine Liste mit ganzen Zahlen von 1 bis 10. Gib alle Zahlen der Liste aus.
#   5. Gib alle geraden und dann alle ungeraden Zahlen in der Liste aus. Gib nur die ersten und dann nur die letzten vier Elemente der Liste aus.
# 2. **Tabellen**
#   1. Lade die Tabelle "titanic.csv" mit Hilfe der Funktion ```read_csv()``` der Bibliothek ```Pandas``` in ein DataFrame. <font color='green'>**HINWEIS:** Die Tabelle findest du im StudIP Verzeichnis dieser Übung zum Download. Du kannst sie mit Hilfe des ```Upload```-Buttons (im Dashboard direkt neben ```new```) in deinen Arbeitsbereich laden. Es ist hilfreich, wenn du in deinem Arbeitsbereich einen neuen Ordner ```daten``` erstellst, und diesen und alle zukünftigen Datensätze dort speicherst. Um eine Bibliothek zu benutzen, musst du sie erst mit Hilfe des ```import``` Keywords importieren!</font>
#   2. Lass dir die ersten paar Zeilen der Tabelle anzeigen und finde heraus, wieviele Einträge die Tabelle hat. <font color='green'>**HINWEIS:** benutze die Funktionen ```head()``` und ```len()```.</font>
#   3. Finde heraus, was die einzelnen Spalten in dem Datensatz für eine Bedeutung haben. Bei einigen, wie z.B. ```age``` ist das klar, aber was bedeutet zum Beispiel ```sibsp```?
#   4. Berechne den durchschnittlich bezahlen Ticket-Preis.
#   5. **(Optional):** Wieviele Passagier*innen haben überlebt?
#   6. **(Optional):** Erstelle eine Liste mit ganzen Zahlen zwischen 0 und 100 mit Hilfe der ```range()```-Funktion und der ```list()```-Funktion. Gib die Länge der Liste (Stichwort: ```len()```) aus. <font color='green'>**HINWEIS**: Wenn du auf diese Weise listen erstellst, musst du das mit ```range()``` generierte Objekt mit Hilfe von ```list()``` noch in eine Liste verwanden. ```range()``` erzeugt nämlich erst einmal nur einen "Generator", der eine Liste sein _könnte_. Erst durch ```list()``` wird der Generator explizit in eine Liste verwandelt. Du kannst dir dieses Verhalten genauer ansehen, indem du mit ```type()``` den Typ der jeweils erzeuten Objekte ansiehst:</font>

# +
generator = range(0,5)
print(type(generator))

neue_liste = list(generator)
print(type(neue_liste))
# -

# <font color='green'>Dieses Verhalten ist jetzt nicht besonders wichtig und du musst es dir auch nicht merken, aber wir erwähnen es hier der Vollständigkeit halber.</font>
#
# G. **(Optional)** Erstelle eine Liste mit 5 beliebigen Zahlen. Iteriere über die Liste und addiere zu jeder Zahl die Summe aller Elemente der Liste. Speichere das Ergebnis in einer neuen Liste.  
#     <font color='green'>**HINWEIS**: Die Funktion ```sum()``` berechnet automatisch die Summe der Elemente einer Liste. Eine neue Liste erzeugen und ihr Elemente hinzufügen funktioniert wie folgt:</font>

# +
# HINWEIS
# eine leere Liste erzeugen
leere_liste = []
print(leere_liste)

# der leeren Liste ein neues Element hinzufügen
leere_liste.append(2)
print(leere_liste)
# -

# H. **(Optional)** Setze jedes zweite Element der Liste aus Aufgabe F gleich null indem du ein slice-Operation benutzt.  
#
#   <font color='green'>**HINWEIS 1**: Ein einzelnes Element einer Liste wird folgendermaßen verändert:</font>

# +
# erstelle eine neue Liste und gib sie aus
test_liste = [1,2,3,4,5]
print(test_liste)

# ersetze das Element an Index 2 mit der Zahl 100
test_liste[2] = 100
print(test_liste)
# -

# <font color='green'>**HINWEIS 2**: Listen mit sich wiederholenden Elementen können auch folgendermaßen erzeugt werden:</font>

# +
# das "Produkt" einer Liste mit einer Zahl x
# ist die x-fache Wiederholung aller Elemente der Liste

# eine Liste mit nur einem Element
neue_liste = [0] * 5 
print(neue_liste)

# eine Liste mit zwei Elementen
neue_liste = [1, 2] * 3 
print(neue_liste)

# eine Liste mit 5 Elementen
neue_liste = [1, 2, 3, 4, 5] * 2
print(neue_liste)
