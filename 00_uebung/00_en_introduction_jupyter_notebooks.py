# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _This teaching material can be freely used, distributed and modified as per the [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) license._   
#  _Authors: Jana Lasser (jana.lasser@ds.mpg.de), Benjamin Säfken (benjamin.saefken@wiwi.uni-goettingen.de), Debsankha Manik (debsankha.manik@ds.mpg.de)._  
#  _This teaching material is created for the project "Daten Lesen Lernen", funded by Stifterverband and the Heinz Nixdorf Stiftung._

# <a name="top"></a>Introduction to Jupyter Notebooks
# ===
#
# * [Why Jupyter notebooks?](#warumnotebooks)
# * [How to use Jupyter notebooks?](#wienotebooks)
#     * [The dashboard](#dashboard)
#     * [The modal Editor](#editor)
#     * [Cell types](#zelltypen)
#     * [Markdown in Text cells](#markdown)
#     * [Important commands](#kommandos)
#     * [Summary and exercise](#zusammenfassung)
#
# ### The Goal
# During the course "Daten Lesen Lernen" we will use  [Jupyter notebooks](https://jupyter.org/) as teaching resources as well as as the tool of choice for working on the tasks for the tutorials. In the following, we would like to get you ready for working with Jupyter notebooks for programming in python.
# When you are finished, you will be:
# * familiar with the structure of a notebook,
# * able to create a new notebook and
# * distinguish between different cell types and switch between them.
#
# <a name="warumnotebooks"></a>Why Jupyter notebooks?
# ---
# A Jupyter notebook is a simple programming environment that allows the user to put together in a single file structured text (with headings, tables, formulae...), visualizations as well as executable code. For example, [this notebook](https://ocefpaf.github.io/python4oceanographers/blog/2015/07/13/interactive_geo/) illustrates an use case for geosciences. Two great advantages for this are
# * we can administer the programming environment centrally and homogeneously (no need to install software on each of your PC, a modern browser is enough) and
# * you can directly modify and extend the teaching material any way you wish. 
#
# We hope that this toolbox will provide those of you who have no prior experience of programming an easy entry point to learn it, as well as try out newly learnt concepts very easily.
#
# [Top](#top)

# <a name="wienotebooks"></a>How to use a Jupyter notebook?
# ---
#
# <a name="dashboard"></a> **The Dashboard**
#
# The "Dashbboard" is the index of the Jupyter notebooks. Clicking on the "Jupyter" logo at the upper left corner brings us to the dashboard. Here you can open existing notebooks and create new ones. To create a new notebook:
# * click on "New" in the dashboard at upper right corner,
# * choose language "Python 3",
# * give the notebook a name by clicking on the text field that reads "Untitled" on upper left.
#
# <a name="editor"></a> **The modal Editor**
#
# The Jupyter notebook has a so-called _modal editor_. It understands two modes: 
# * the **Edit Mode** to write _inside_ the cells and 
# * the **Command Mode** to work with a cell or a group of cells. 
#
# A   <font color='green'>_green_</font> border around a cell denotes that we are in **Edit Mode**, and a <font color='blue'>_blue_</font> border means we are in **Command Mode**.
#
# To switch between these two modes:
# * **Edit Mode**: press ```Enter``` or click *inside* a cell.
# * **Command Mode**: press ```Esc``` or click _outside_ the cells.
#
# <a name="zelltypen"></a> **Cell types**
#
# A Jupyter notebook understands _cells_ with different types of content:
# * so called **Markdown cells**: they contain structured **Text**, in a format called "Markdown". Apart from normal text, such a cell may contain headings, lists, tables etc. 
# *  **Code cells** (distinguishable by the ```In [1]:``` at the left of the cell and the light gray background. 

# Example of a code cell
# Execute me by pressing `SHIFT+ENTER`.
a = 10
print(a)

# Here's how you can switch between the two cell types:
# * **Code**: press ```y``` (in command mode) or click on the menu bar at the top: _Cell_ $\rightarrow$ _Cell Type_ $\rightarrow$ _Code_
# * **Text**: press ```m``` (in command mode) or  click on the menu bar at the top: _Cell_ $\rightarrow$ _Cell Type_ $\rightarrow$ _Markdown_
#
# [Top](#top)
#
# <a name="markdown"></a> **Markdown in text cells**  
#
# Text cells allows us to write *structured* text with headings, lists, tables and mathematical formulae. To add structure to our text, we need to augment it with additional *formatting*. For example, to create a heading, we need to add a ```# ``` (pound symbol followed by a space) before the header text. Similar to code cells, a markdown cell can also be *executed* by pressing `SHIFT+ENTER`. When we do that, the text will be *formatted* nicely. At this point, the notebook will switch to _command_ mode. To edit the text again, we need to switch to _edit_ mode. Many useful markdown commands are listed [here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).
#
# <a name="kommandos"></a> **The important commands**
#
#
# All commands can be executed by clicking with the mouse on the toolbar at the top. Nevertheless, it is useful to learn a few keyboard shortcuts in order to make one's life easier while working with Jupyter notebooks. The most important of these shortcuts are:
# * In <font color='blue'>**command Mode**</font>:
#     * ```h``` to get a list of shortcuts. 
#     * ```a``` and ```b``` to insert a new cell above/below the current cell. 
#     * ```d-d``` to delete the selected cell (to *undelete* a cell, click on the menu bar _Edit_ $\rightarrow$  _Undo Delete Cells_). 
#     * ```shift-enter``` to execute the selected cell. 
#     
#     
# * In <font color='green'>**edit Mode**</font>:
#     * ```tab``` for text completion.
#     * ```ctrl-c``` to copy the content of a cell. 
#     * ```ctrl-v``` to paste as content of a cell. 
#     * ```ctrl-z``` to undo the last action.

# <a name="zusammenfassung"></a> **Summary and exercise**
#
# A Jupyter notebooks is a mixture of a document and a programming environment. One can store structured text and executable code together in a notebook. A few keyboard shortcuts makes ones life easier while working with notebooks. The dashboard lists all notebooks, just like a PC's file browser lists the files in a directory. 

#   1. Navigate to the dashboard (preferably by right clicking on the Jupyter logo $\rightarrow$ "Open Link in New Tab")
#   2. Create a new notebook as an exercise. 
#   3. Give the notebook a proper name (e.g.) "exercise01".
#   4. **(Optional)** Copy a few example code from the [Matplotlib Gallerie](https://matplotlib.org/2.1.1/gallery/index.html) in the new notebook, execute the code cells (`SHIFT+ENTER`) and try to modify some bits to change the generated plots.

# [Top](#top)
