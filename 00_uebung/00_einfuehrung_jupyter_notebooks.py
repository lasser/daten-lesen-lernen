# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _Das folgende Lehrmaterial kann unter einer [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) Lizenz frei verwendet, verbreitet und modifiziert werden._   
#  _Authoren: Jana Lasser (jana.lasser@ds.mpg.de), Benjamin Säfken (benjamin.saefken@wiwi.uni-goettingen.de)._  
#  _Das Lehrmaterial wurde im Zuge des Projektes "Daten Lesen Lernen", gefördert vom Stifterverband und der Heinz Nixdorf Stiftung erstellt._

# <a name="top"></a>Einführung Jupyter-Notebooks
# ===
#
# * [Warum Jupyter-Notebooks?](#warumnotebooks)
# * [Wie benutzt man Jupyter-Notebooks?](#wienotebooks)
#     * [Das Dashboard](#dashboard)
#     * [Der modale Editor](#editor)
#     * [Zelltypen](#zelltypen)
#     * [Markdown in Text-Zellen](#markdown)
#     * [Die wichtigsten Kommandos](#kommandos)
#     * [Zusammenfassung & Übung](#zusammenfassung)
#
# ### Lernziel
# Im Verlauf der Vorlesung "Daten Lesen Lernen" werden wir [Jupyter-Notebooks](https://jupyter.org/) sowohl als Lehrmaterial als auch als Werkzeug für die Bearbeitung von Aufgaben in den Tutorien einsetzen. Mit dieser kurzen Lerneinheit möchten wir Sie darauf vorbereiten, wie man Jupyter-Notebooks zum Programmieren mit Python einsetzt.
# Am Ende dieser Lerneinheit sollt ihr
# * mit dem Aufbau von Notebooks vertraut sein,
# * selbstständig ein neues Notebook anlegen können und
# * zwischen verschiedenen Zelltypen unterscheiden und umschalten können.
#
# <a name="warumnotebooks"></a>Warum Jupyter-Notebooks?
# ---
# Ein Jupyter-Notebook ist eine einfache Programmierumgebung, die es erlaubt, strukturierten Text (Überschriften, Tabellen, Formeln...) mit Abbildungen und ausführbarem Code zu vermischen. Das ist beispielsweise [in diesem Notebook](https://ocefpaf.github.io/python4oceanographers/blog/2015/07/13/interactive_geo/) für eine Anwendung aus den Geowissenschaften illustriert. Zwei große Vorteile liegen darin dass
# * die Programmierumgebung von uns zentral und homogen vorgehalten werden kann (Einrichten auf den PCs der Studierenden entfällt, ein Browser reicht zur Benutzung) und
# * dass die Unterrichtsmaterialien selbst von den Studierenden direkt adaptierbar und weiterverwendbar sind.
#
# Wir erhoffen uns, dass mit diesem Werkzeug selbst Studierende ohne jegliche Vorerfahrung mit Programmierung einen leichten Einstieg finden und gelernte Konzepte direkt ausprobieren können.
#
# [Anfang](#top)

# <a name="wienotebooks"></a>Wie benutzt man ein Jupyter-Notebook?
# ---
#
# <a name="dashboard"></a> **Das Dashboard**
#
# Das "Dashbboard" ist das Heimverzeichnis des Jupyter Notebooks. Zum Dashboard gelangt man durch Klick auf das "Jupyter" Logo in der linken oberen Ecke. Dort kann man andere Notebooks öffnen und neue erstellen. Sie können ein neues Notebook erstellen durch
# * klick auf "new" im Dashboard oben rechts,
# * als Sprache "Python 3" auswählen und
# * dem Notebook durch klick in den Schriftzug "untitled" einen neuen Namen geben.
#
# <a name="editor"></a> **Der modale Editor**
#
# Das Jupyter Notebook ist ein sogenannter _modaler Editor_. Es kennt zwei Modi: 
# * den **Edit Mode** zum Schreiben _innerhalb_ von Zellen und 
# * den **Command Mode** zum Bearbeiten von Zellblöcken. 
#
# Der **Edit Mode** ist durch die <font color='green'>_grüne_</font>, der **Command Mode** durch die <font color='blue'>_blaue_</font> Zellumrandung erkennbar.
#
# Um zwischen den Modi zu wechseln:
# * **Edit Mode**: drücke ```Enter``` oder klicke *in* eine Zelle
# * **Command Mode**: drücke ```Esc``` oder klicke _neben_ die Zelle
#
# <a name="zelltypen"></a> **Zell-Typen**
#
# Ein Jupyter-Notebook kennt _Zellen_ mit verschiedenen Inhalten:
# * strukturierter **Text**, auch "Markdown" genannt. Dieser enthält Überschriften, Fließtext, Listen etc.
# * und **Code** (erkennbar an dem ```In [1]:``` vor der Zelle und dem hellgrauen Zellhintergrund).

# Beispiel einer Code-Zelle
# tippen Sie bitte SHIFT+ENTER, um mich auszuführen
a = 10
print(a)

# Um den Typ des Inhalts einer Zelle zu wechseln:
# * **Code**: tippen Sie ```y``` (im Command Mode) oder klicken Sie im Menü oben _Cell_ $\rightarrow$ _Cell Type_ $\rightarrow$ _Code_
# * **Text**: tippen Sie ```m``` (im Command Mode) oder klicken Sie im Menü _Cell_ $\rightarrow$ _Cell Type_ $\rightarrow$ _Markdown_
#
# [Anfang](#top)
#
# <a name="markdown"></a> **Markdown in Text-Zellen**  
#
# Text-Zellen erlauben eine _strukturierte_ Darstellung von Text mit Überschriften, Listen, Tabellen und sogar mathematischen Formeln. Um Text zu strukturieren, wird der Text mit Kontrollelementen versehen versehen. Um z.B. eine Überschrift zu erstellen, wird vor den Text ein oder mehrere Raute-Zeichen (```#```) gestellt. Ähnlich den Code-Zellen wird die Text-Zelle "ausgeführt" und verwandelt sich dadurch in schön formatierten Text. Im _edit_-Mode kann der Text wieder modifiziert werden. Viele hilfreiche Markdown-Kommands sind [hier](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) aufgelistet.
#
# <a name="kommandos"></a> **Die wichtigsten Kommandos**
#
#
# Alle Kommandos können mit der Maus in der Toolbar (oben) angeklickt werden. Trotzdem lohnt es sich 4-5 Tastenkürzel zu kennen, , da dies die Arbeit mit diesem Werkzeug um einiges effizienter macht. Die wichtigsten sind:
# * Im <font color='blue'>**command Mode**</font>:
#     * ```h``` für eine Liste der Kommandos
#     * ```a``` und ```b``` für neue Zelle über/unter der ausgewählten Zelle
#     * ```d-d``` um die ausgewählte Zelle zu löschen (zum Wiederherstellen, klicken Sie im Menü oben _Edit_ $\rightarrow$  _Undo Delete Cells_)
#     * ```shift-enter``` um die ausgewählte Zelle auszuführen 
#     
#     
# * Im <font color='green'>**edit Mode**</font>:
#     * ```tab``` für Textvervollständigung
#     * ```ctrl-c``` zum Kopieren von Inhalten in einer Zelle
#     * ```ctrl-v``` zum Einfügen von Inhalten in einer Zelle
#     * ```ctrl-z``` um die letzte Aktion rückgängig zu machen

# <a name="zusammenfassung"></a> **Zusammenfassung & Übung**
#
# Jupyter-Notebooks sind eine Mischung aus Dokument und Programmierumgebung. In einem Notebook lassen sich strukturierter Text und ausführbarer Code vermischt darstellen. Die Funktion innerhalb eines Notebooks wird am besten durch ein paar einfache Tastenkürzel gesteuert. Notebooks lassen sich wie Dateien im Dashboard verwalten. 

#   1. Navigieren Sie zum Dashboard (am besten Rechtsklick auf das Jupyter Logo $\rightarrow$ "in neuem Tab öffnen")
#   2. Erstellen Sie ein neues Notebook für die Übung
#   3. Geben Sie dem Notebook einen bezeichnenden Namen (z.B.) "Uebung01".
#   4. **(Optional)** Kopieren Sie ein Stück Beispielcode aus der [Matplotlib Gallerie](https://matplotlib.org/2.1.1/gallery/index.html) in das neu erstellte Notebook, führen Sie den Code aus und versuchen sie, den Code zu modifizieren um die Abbildung zu modifizieren.

# [Anfang](#top)
