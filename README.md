# daten-lesen-lernen

Übungen und Lösungen für die im Rahmen der Vorlesung "Daten Lesen Lernen" gehaltenen Tutorien.

# Managing Jupyter notebooks via jupytext
In order to avoid merge conflicts with jupyter notebooks, one can use
[jupytext](https://github.com/mwouts/jupytext). To set it up:

1. Work in a virtual environment (important).
2. From inside the virtual environment, run the installation script
```bash
source install.sh
```
3. Done!

## What is happening
* At each `git commit`, jupyter notebooks automatically get converted to
  a plaintext (py:light) format. This is what git sees from now on, ignoring the ipynb files.
* At each `git pull`, py:light files jupyter notebooks get converted
  into "proper" notebooks. 
* So in ideal case, an user can ignore all these py_light<->ipynb convertions happening 
  behind the scenes and keep on doing `git add bla.ipynb; git commit; git push` as before.
* If these automagical conversions fail in any case, one can do them
  manually:
```bash
# The following converts ipynb to plaintext. Later normal `git add/commit/push`
# trinity can follow.
convert_ipynb2py foo.ipynb # generates too.py
# The following converts plaintext files to jupyter notebooks. May be needed if after a git pull,
# you don't see the expected jupyter notebooks for some reasons.
convert_py2ipynb foo.py # generates foo.ipynb
```

## In a new repo:
Initially convert all .py files to .ipynb files again
```bash
for file in $(ls *.py); do convert_py2ipynb $file; done
```
