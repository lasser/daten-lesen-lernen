# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Analyse von Daten um den Ursachen für Unterernährung in Zambia auf die Spur zu kommen
#
# [Unterernährung](https://de.wikipedia.org/wiki/Unterern%C3%A4hrung) kann, besonders im Kindesalter, zum Zurückbleiben in der körperlichen und geistigen Entwicklung (Untergewicht, Kleinwuchs, kognitive Retardierung), zu schweren Krankheiten und im Extremfall zum Tod führen. Um Hinweise auf die Ursachen von Unterernährung zu bekommen, führt die [WHO Stichproben-Studien](https://www.who.int/news-room/fact-sheets/detail/malnutrition) in Ländern durch, die von Unterernährung bei Kindern betroffen sind. In diesen Studien wird der Ernährungs- bzw. Entwicklungsstatus der Kinder mit dem sog. [Z-Score](http://conflict.lshtm.ac.uk/page_125.htm) erfasst und mit anderen Beobachtungen wie z.B. dem Alter und BMI der Mutter und der Stilldauer verknüpft. In diesem Projekt soll analysiert werden, welche Faktoren die Unterernährung bei Kindern am meisten beeinflussen.
#   
# ## Der Datensatz
# Der Datensatz ist eine .csv Datei mit der folgenden Struktur:
#
# |zscore|c_gender|c_breastf|c_age|m_agebirth|m_height|m_bmi|m_education|m_work|region|district|
# |----|------|----|---|---|----------|---------|-------|------|---|
# |-159|0|0|4|24.6|148.9|21.83|2|0|1|81|
#
# Jede Zeile beschreibt ein Kind und seine Mutter, der Datensatz wurde im Jahr 1992 erhoben. Die Spalten beinhalten Information zu
#
# |Spalte| Beschreibung|
# |------|------------|
# |zscore | der Z-Score des Kindes |
# |c_gender| Geschlecht des Kindes: 1 = männlich, 0 = weiblich |
# |c_breastf| Stilldauer in Monaten|
# |c_age| Alter des Kindes in Monaten|
# |m_agebirth| Alter der Mutter in Jahren|
# |m_height| Größe der Mutter in Zentimetern|
# |m_bmi| Body Mass Index der Mutter|
# |m_education| Bildungsniveau der Mutter: 1 = keine Bildung, 2 = Grundschule, 3 = Sekundarschule, 4 = Hochschulbildung |
# |m_work| Arbeitsstatus der Mutter: 1 = Mutter arbeitet, 0 = Mutter arbeitet nicht|
# |region| Heimatregion in Zambia: 1 = Central, 2 = Copperbelt, 3 = Eastern, 4 = Luapula, 5 = Lusaka, 6 = Northern, 7 = North western, 8 = Southern, 9 = Western|
# |district|Heimatdistrikt in Zambia (55 Distrikte)|

# ## Aufgaben
# 1. Was ist der Z-Score und wie wird er berechnet?
# 2. Welche Informationen enthält der Datensatz? Berechne Mittelwert, Median und Standardabweichungen der Größen, für die so eine Berechnung Sinn ergibt.
# 3. Visualisiere die Verteilung der verschiedenen Beobachtungen z. B. in Histogrammen, Balkendiagrammen oder [Tortendiagrammen](https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.pie.html).
# 4. Stelle eine Hypothese auf und überprüfe sie:
#     * Welche(r) Faktor(en) hat/haben den größten Einfluss auf Unterernährung bei Kindern?
#     * Überprüfe deine Hypothese anhand einer linearen Regression. Was sind die Ergebnisse? Wie interpretierst du sie? 
#     * Stelle die Regression auch graphisch anhand eines Liniendiagrams dar. 
# 6. Kannst du die Intensität der Unterernährung für die Verschiedenen Regionen (oder Distrikte) anhand einer Landkarte darstellen? (Siehe z. B. [diesen Artikel](https://towardsdatascience.com/lets-make-a-map-using-geopandas-pandas-and-matplotlib-to-make-a-chloropleth-map-dddc31c1983d) für eine Beschreibung, wie so etwas gehen könnte.)
# 7. (optional) Das DHS-Programm bietet in seiner [Datenbank](https://dhsprogram.com/data/available-datasets.cfm) Erhebungen zu unterschiedlichen Zeitpunkten an. Kannst du daraus eine Zeitreihe konstruieren und untersuchen, wie sich die Unterernährung in Zambia über die Zeit entwickelt? 
#     * Identifiziere dazu zuerst die geeigneten Datensätze. 
#     * Berechne den Anteil an Kindern, die unterernährt sind zu jedem erhobenen Zeitpunkt. 
#     * Stelle die Entwicklung anhand eines Linien-Diagrams dar.
# 8. (optional) Jetzt, wo du eine Vorgehensweise für die Datenanalyse anhand der Daten von Zambia etabliert hast: Analysiere die zeitliche Entwicklung der Unterernährung in einem anderen Land (oder mehreren anderen Ländern) deiner Wahl. Die Daten für 50 andere Länder sind ebenfalls in der DHS-Datenbank hinterlegt. Vergleiche die Ergebnisse mit Zambia.
