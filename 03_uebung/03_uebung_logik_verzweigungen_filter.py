# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _Lizenz: Das folgende Lehrmaterial kann unter einer [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) Lizenz frei verwendet, verbreitet und modifiziert werden._   
#  _Authoren: Jana Lasser (jana.lasser@ds.mpg.de)_  
#  _Das Lehrmaterial wurde im Zuge des Projektes "Daten Lesen Lernen", gefördert vom Stifterverband und der Heinz Nixdorf Stiftung erstellt._

# # Übung 3
# ### Praktische Hinweise
# $\rightarrow$ Übungen sind dafür gedacht, dass ihr sie euch daheim anseht und versucht, ein paar Aufgaben selbst zu lösen. In den Tutorien könnt ihr euch weiter mit dem Lösungen der Aufgaben beschäftigen und dabei Hilfe von den Tutor*innen bekommen.  
#
# $\rightarrow$ Wenn ein Fehler auftritt:
# 1. Fehlermeldung _lesen_ und _verstehen_
# 2. Versuchen, selbst eine Lösung zu finden ($\rightarrow$ hier findet das Lernen statt!)
# 3. Das Problem googlen (Stichwort: Stackoverflow) oder den/die Nachbar*in fragen
# 4. Tutor*in fragen
#
# $\rightarrow$ Unter <font color='green'>**HINWEIS**</font> werden Hinweise gegeben, die für die Lösung der Aufgabe hilfreich sind und oftmals auch weiterführende Informationen zur Aufgabe liefern.
#
# $\rightarrow$ Mit **(Optional)** gekennzeichnete Übungsteile sind für besonders schnelle Menschen :-).

# ### Logik, Verzweigungen und Filter
# 1. **Logik**
#   1. Schreibe ein Programm, das überprüft, ob ein Wort den Buchstaben 'e' enthält und kürzer als 5 Buchstaben ist.
#   2. Schreibe ein Programm, das überprüft ob eine Zahl gerade oder ungerade ist. 
#   <font color='green'>**HINWEIS**: mit dem Modulo-Operator ```%``` lässt sich "Teilen mit Rest" realisieren. Der "Rest" der Division wird zurückgeliefert:</font>
#   

print(11 % 2)
print(10 % 2)

# 2. **Verzweigungen**
#   1. Erstelle eine Liste mit den Zahlen von 0 bis 20. <font color='green'>**HINWEIS**: Benutze die ```range()```-Funktion!</font>
#   2. Schreibe ein Programm, das mit einem ```for``` loop über die Liste iteriert und
#     * alle Zahlen kleiner 10 mit 2 multipliziert und ausgiebt
#     * alle Zahlen größer 10 durch 2 teilt und ausgiebt
#     * einen Spezialfall für die Zahl 15 hat und eine Warnung ausgibt, wenn diese Zahl erreicht wird.  
#     
#   <font color='green'>**HINWEIS:** Die Keywords die du benötigen wirs sind ```if```, ```elif``` und ```else```. </font>
# 3. **Filter (Überlebende der Titanic nach Geschlecht)** 
#   <font color='green'>**HINWEIS:** Wenn du das Filtern eines DataFrames noch nicht ganz durchblickt hast oder es in der Vorlesung zu kurz gekommen ist, hilft dir der untenstehende kleine Exkurs, der die Vorgehensweise noch einmal an einem sehr einfachen Beispiel erläutert.</font>  
#   
#   1. Lade, wie schon in Übung 2, Aufgabe 2.A den Titanic-Datensatz in ein ```Pandas``` DataFrame. Erzeuge eine Tabelle, die nur die Einträge von überlebenden Passagier\*innen enthält. Wieviele sind es jeweils?
#   2. Erzeuge basierend auf der Tabelle mit Überlebenden zwei weitere Tabellen, die nur die überlebenden Männer bzw. Frauen enthalten.
#   3. Finde heraus, wie viele Frauen und Männer ursprünglich an Board waren und wieviele jeweils Überlebt haben.
#   4. Was war für einen Mann oder eine Frau an Board der Titanic die Chance, zu überleben? 
#   5. Wie kannst du dir erklären, dass es so einen großen Unterschied zwischen den Geschlechtern gibt? Wie kannst du dein historisches oder gesellschaftliches Wissen (domain knowledge) benutzen, um eine Kausalität zwischen Geschlecht und Überlebenschance herzustellen? Was für alternative Erklärungen könnte es geben, wenn du dein domain knowledge nicht verwendest?
#   6. **(Optional)**: mache die selbe Auswertung wie oben, nur diesmal nach Klasse (```pclass``` oder ```class```). Was für Unterschiede gibt es?

# ### Exkurs: filtern eines DataFrames
# Ein DataFrame nach Einträgen zu filtern, die einem bestimmten Kriterium entsprechen, ist eine extrem wichtige Technik in der Datenanalyse.

# +
# importiere Pandas unter dem Kürzel "pd"
import pandas as pd

# wir erstellen eine Tabelle zum Testen, die Namen
# (Strings) und Alter (Zahlen) enthält 

# zuerst erstellen wir die Liste "namen" und
# die Liste "alter"
namen = ['tim','olaf','nina','andrea','jana', 'miro']
alter = [31, 52, 14, 46, 28, 3]

# dann erstellen wir die Tabelle "personen"
# (ein DataFrame) und übergeben die beiden 
# Listen als Spalten der Tabelle
personen = pd.DataFrame({'name':namen, 'alter':alter})

# schließlich zeigen wir die ersten sechs Einträge
# der Tabelle an
personen.head(6)
# -

# Um nur bestimmte Einträge der Tabelle anzuzeigen können wir eine "Maske" definieren, die festlegt, welche Zeilen angezeigt werden und welche nicht. Die Maske besteht aus einer Liste von Wahrheitswerten (```True``` und ```False```), die genauso viele Einträge umfasst, wie die Tabelle Zeilen hat:

# +
# hier erstellen wir die Maske
maske = [False, True, False, True, False, True]

# wir können die Maske jetzt benutzen, um nur gewisse
# einträge in der Tabelle auszuwählen:
personen[maske]
# -

# natürlich können wir die so "gefilterte" Tabelle
# auch in einer neuen Variable speichern
gefilterte_tabelle = personen[maske]
gefilterte_tabelle.head()

# Als nächsten Schritt erzeugen wir auch die Maske automatisch. Wir möchten wissen, welche Personen in der Tabelle älter als 30 Jahre sind. Das fragen wir mit Hilfe des "größer-oder-gleich" Operators ```>=``` ab:

personen['alter'] >= 30

# +
# diese Maske speichern wir wieder in 
# einer Variablen
maske_alt = personen['alter'] >= 30

# und filtern die Tabelle mit Hilfe der
# neu erstellten Maske
alte_leute = personen[maske_alt]
alte_leute.head()
# -

# Genauso können wir natürlich auch die jungen Leute finden, indem wir den "kleiner"-Operator ```<``` einsetzen:

# +
# erstelle die Maske
maske_jung = personen['alter'] < 30

# filtere die Tabelle
junge_leute = personen[maske_jung]
junge_leute
# -

# Oder die Personen mit kurzen Namen (wir benutzen den "kleiner-oder-gleich"-Operator ```<=```):

# +
# überprüfe die Länge des Strings in der Spalte "name"
maske_kurzer_name = personen['name'].str.len() <= 4

# filtere die Tabelle mit der Maske
kurzer_name = personen[maske_kurzer_name]
kurzer_name.head()
# -

# Die selben Prinzipien kannst du auf jedes DataFrame und insbesondere auch auf den Titanic-Datensatz anwenden, um nach bestimmten Eigenschaften der Passagier*innen zu filtern und interessante Erkenntnisse zu gewinnen.
