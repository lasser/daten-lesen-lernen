# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _Lizenz: Das folgende Lehrmaterial kann unter einer [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) Lizenz frei verwendet, verbreitet und modifiziert werden._   
#  _Authoren: Jana Lasser (jana.lasser@ds.mpg.de)_  
#  _Das Lehrmaterial wurde im Zuge des Projektes "Daten Lesen Lernen", gefördert vom Stifterverband und der Heinz Nixdorf Stiftung erstellt._

# # Übung 1
# ### Praktische Hinweise
# $\rightarrow$ Übungen sind dafür gedacht, dass ihr sie euch daheim anseht und versucht, ein paar Aufgaben selbst zu lösen. In den Tutorien könnt ihr euch weiter mit dem Lösungen der Aufgaben beschäftigen und dabei Hilfe von den Tutor*innen bekommen.  
#
# $\rightarrow$ Wenn ein Fehler auftritt:
# 1. Fehlermeldung _lesen_ und _verstehen_
# 2. Versuchen, selbst eine Lösung zu finden ($\rightarrow$ hier findet das Lernen statt!)
# 3. Das Problem googlen (Stichwort: Stackoverflow) oder den/die Nachbar*in fragen
# 4. Tutor*in fragen
#
# $\rightarrow$ Unter <font color='green'>**HINWEIS**</font> werden Hinweise gegeben, die für die Lösung der Aufgabe hilfreich sind und oftmals auch weiterführende Informationen zur Aufgabe liefern.
#
# $\rightarrow$ Mit **(Optional)** gekennzeichnete Übungsteile sind für besonders schnelle Menschen :-).

# ### Python als Taschenrechner
# 1. **Vorbereitung**
#   1. Navigiere zum Dashboard (am besten Rechtsklick $\rightarrow$ "in neuem Tab öffnen")
#   2. Erstelle ein neues Notebook für die Übung: "new" (rechts oben) $\rightarrow$ "Python 3"
#   3. Gib dem Notebook einen bezeichnenden Namen (z.B.) "Uebung01" indem du oben links in das "Untitled" Feld klickst.
# 2. **Zahlen**
#   1. Schreibe ein Programm, das die Resultate von Rechnungen ausgibt (```print()```). Benutze jede der standard Rechenoperationen (+, - , *, /, \**) mindestens ein mal.
#   2. Benutze mehrere Rechenoperationen in einer Rechnung, benutze Klammern um die Reihenfolge der Operationen zu verändern.
#   3. **(Optional)** Realisiere eine Rechnung "ohne Rest" (```int()```) und lass dir den Typ der Ergebnisse der Rechnungen mittels ```type()``` anzeigen.
#   4. **(Optional)** Recherchiere was es mit "numerischer Genauigkeit" auf sich hat und wo sie zu Problemen führen könnte. Programmiere ein kleines Beispiel. 
# 3. **Strings**
#   1. Such dir eine*n bekannte*n Wissenschaftler*in (natürlich aus Göttingen...). Speichere Vorname, Nachname und ein Zitat des/der Wissenschaftlers/in in drei separaten Variablen.
#   2. Addiere die Strings (Concatenation) um einen Satz mit dem Namen des/der Wissenschaftlers*in und dem Zitat zu erzeugen und auszugeben.
#   3. Formatiere den Output mit der ```format()``` Funktion so, dass er Sinn ergibt und gut lesbar ist (Stichwort Leerzeichen und Satzzeichen).
# 4. **(Optional) Kommentare**
#   1. Schreibe ein Programm, das jedes neue Konzept das du bis jetzt gelernt hast verwendet.
#   2. Kommentiere das Programm ausführlich um zu beschreiben, was es tut.  
#   
#   <font color='green'>**HINWEIS:** ein Kommentar in einer Code-Zelle wird durch den Hashtag ```#``` eingeleitet. Der Text hinter dem Hashtag wird nicht ausgeführt sondern dient nur zur Beschreibung des Codes drumherum.</font>

# +
# Beispiel
# Kommentare können sowohl eine eigene Zeile sein
print(2+2)  # oder aber neben ausgeführtem Code stehen

print('im Gegensatz zu Code werden Kommentare nicht ausgeführt')
# -

#    C. **(Optional)** Für jede neue Aktion die das Programm ausführt, schreibe Output (```print()```) der erklärt, was gerade passiert ist.
